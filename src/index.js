/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import ReactDOM from "react-dom";
import {browserHistory, Router, Route, IndexRoute} from 'react-router'
import {Provider} from "react-redux"
import store from "./js/store"

import routes from './js/routes';

const app = document.getElementById('app');

ReactDOM.render(
  <Provider store={store}>
    <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory} routes={routes}/>
  </Provider>,
  app);