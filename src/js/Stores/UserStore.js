/**
 * Created by luardo on 30/10/16.
 */
import {EventEmitter} from "events";
import dispatcher from "../dispatcher"

class UserStore extends EventEmitter {
    constructor() {
        super();

        this.user = [];
        this.company = [];
        this.locations = [];
        this.location = [];

    }

    add(response) {
        this.user = response.data.user;
    }

    getLocations() {
        return this.locations;
    }

    getLocation() {
        return this.location;
    }


    getUser() {
        return this.user;
    }

    getCompanyById(id) {
        return this.company;
    }

    getUserById(id) {
        return this.user;
    }

    handleActions(action) {
        switch (action.type) {
            case "CREATE_USER":
                this.user = action.user;
                this.emit("change");
                break;
            case "RECEIVE_COMPANY_DATA":
                this.company = action.company;
                this.emit("change");
                break;
            case "CREATE_LOCATION":
                this.location = action.location;
                break;
            case "GET_LOCATIONS":
                this.locations = action.locations;
                this.emit("change");
                break;
        }

    }

}

const userStore = new UserStore;

dispatcher.register(userStore.handleActions.bind(userStore));


export default userStore;