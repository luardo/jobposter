/**
 * Created by luardo on 03/11/16.
 */
import {EventEmitter} from "events";
import dispatcher from "../dispatcher"

class SessionStore extends EventEmitter
{
    constructor() {
        super();
        this.errors = [];
        var session = [];


        if(localStorage.session) {
            session = JSON.parse(localStorage.session);
        }

        this.session = session;
    }

    getSession() {
        return this.session;
    }

    saveSession() {
        localStorage.session = JSON.stringify(this.session);
        console.log(localStorage.session);
    }

    handleActions(action) {
        switch (action.type) {
            case "AUTH_PASS":
                this.session = action.sessions;
                localStorage.session = JSON.stringify(this.session);
                this.emit("change");
                break;
            case "AUTH_ERROR":
                this.errors = action.errors;
                this.emit("change");
                break;
            case "FETCH_SESSION_FULFILLED":
                this.session = action.sessions;
                localStorage.session = JSON.stringify(this.session);
                this.emit("change");
        }
    }
}

const sessionStore = new SessionStore;

dispatcher.register(sessionStore.handleActions.bind(sessionStore));


export default sessionStore;