/**
 * Created by luardo on 01/11/16.
 */
import {EventEmitter} from "events";
import dispatcher from "../dispatcher"

class ClassificationsStore extends EventEmitter {
    constructor() {
        super();
        this.classifications = [];
    }

    getClassifications() {
        return this.classifications;
    }

    handleActions(action) {
        switch (action.type) {
            case "RECEIVE_ALL_CLASSIFICATIONS":
                this.classifications = action.classifications;
                this.emit("change");
                break;
        }
    }
}

const classificationsStore = new ClassificationsStore;

dispatcher.register(classificationsStore.handleActions.bind(classificationsStore));

export default classificationsStore;