/**
 * Created by luardo on 01/11/16.
 */
import {EventEmitter} from "events";
import dispatcher from "../dispatcher"

class CreateJobStore extends EventEmitter {
    constructor() {
        super();
        this.channels = [];
        this.classifications = [];
        this.categories = [];
        this.channel = null;
        this.job = {
            'position': '',
            'location_id': '',
            'descriptions_attributes':  [
                {
                    "description_type": "company",
                    "description": "Schöne Musik fängt mit Songs an, die auch jemand kennt. Wir von Songstar entwickeln cloudbasierte Musiksoftware, mit denen wir unsere Kunden im produzieren und im Vertrieb ihrer Songs unterstützen. Zur Ergänzung unseres Teams suchen wir zum nächstmöglichen Zeitpunkt eine/n"
                },
                {
                    "description_type": "job",
                    "description": "<ul><li>In Ihrer Position unterstützen Sie die Vertriebs- und Marketingleitung im täglichen Geschäftsablauf und übernehmen anfallende Bürotätigkeiten\n</li><li>Sie helfen den Teams aus Marketing und Vertrieb bei der Vorbereitung, Durchführung und Auswertung von Projekten\n</li><li>Als erster Ansprechpartner im allgemeinen Schriftverkehr sind Sie verantwortlich für die Koordination der Kommunikation\n</li><li>Selbstständige Marktrecherchen sowie die Pflege von Datenbanken\n</li><li>Vor allem aber gehen Sie mit Spaß und Leidenschaft Ihre Aufgaben an</li></ul>"
                },
                {
                    "description_type": "qualifications",
                    "description": "<ul><li>Erste Berufserfahrungen in ähnlicher Position sind definitiv von Vorteil\n</li><li>Sie wissen wie man auf unterschiedlichen Ebenen kommuniziert und verfügen über ein professionelles Auftreten\n</li><li>Routinierter Umgang mit gängigen Office-Anwendungen\n</li><li>Sie kennzeichnet ein hohes Maß an Eigeninitiative, Musik-Affinität und ein kreativer Kopf\n</li><li>Sehr gute Sprachkenntnisse in deutsch und englisch</li></ul>"
                },
                {
                    "description_type": "additional_information",
                    "description": "<ul><li>Die ideale Kombination aus Musik und Vertrieb\n</li><li>Flexible Arbeitszeiten, leistungsgerechte Entlohnung und Berlin als europäische Kreativ- und Technologiemetropole\n</li><li>Spielraum sich mit unserem Unternehmen zu entwickeln</li></ul>"
                }
            ],
            'hiring_team_member_ids': '',
            'state': '',
            'industry': '',
            'classifications': [],
            'schedules': [],
            'seniority': [],
            'category': '',
            'occupation_list': [],
            'skill_list': '',
            'department_id': '',
            'years_of_experience': [],
            'apply_email': 'aaaaa',
            'apply_url': 'sss',
            'url': '',
            'external_id': '',
            'external_url': '',
            'html': '',
            'ad_template_id': ''
        };
    }

    getChannels() {
        return this.channels;
    }

    getClassifications() {
        return this.job.classifications;
    }

    getSchedules() {
        return this.job.schedules;
    }

    getYearsOfExperience() {
        return this.job.years_of_experience;
    }

    getOccupations() {
        return this.occupations;
    }

    getSeniorities() {
        return this.job.seniorities;
    }

    getCategories() {
        return this.categories;
    }

    handleActions(action) {
        switch (action.type) {
            case "FETCH_CHANNELS":
                this.channels = action.channels;
                this.emit("change");
                break;
            case "FETCH_CHANNEL":
                this.channels = action.channel;
                this.emit("change");
                break;
            case "FETCH_CLASSIFICATIONS":
                this.job.classifications = action.classifications;
                this.emit("change");
                break;
            case "FETCH_SCHEDULES":
                this.job.schedules = action.schedules;
                this.emit("change");
                break;
            case "FETCH_YEARS_OF_EXPERIENCE":
                this.job.years_of_experience = action.years_of_experience;
                this.emit("change");
                break;
            case "FETCH_OCCUPATIONS":
                this.occupations = action.occupations;
                this.emit("change");
                break;
            case "FETCH_SENIORITIES":
                this.job.seniorities = action.seniorities;
                this.emit("change");
                break;
            case "FETCH_CATEGORIES":
                this.categories = action.categories;
                this.emit("change");
                break;
        }
    }
}

const createJobStore = new CreateJobStore;

dispatcher.register(createJobStore.handleActions.bind(createJobStore));

export default createJobStore;