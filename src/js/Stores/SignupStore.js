/**
 * Created by luardo on 29/10/16.
 */
import {EventEmitter} from "events";
import dispatcher from "../dispatcher"


class SignupStore extends EventEmitter {
    constructor() {
        super();
        this.user = [];
        this.company_sizes = [];
        this.industries = [];
        this.signup = {
            "password_confirmation": "",
            "skip_confirmation": "true"
        };
    }

     getUser() {
        return this.user;
    }

    getCompanySizes() {
        return this.company_sizes;
    }

    getIndustries() {
        return this.industries;
    }

    getSignUpData() {
        return this.signup;
    }

    setSignUpData(field, value) {
        var signup = this.getSignUpData();
        signup[field] = value;
        this.signup = signup;
        this.emit("change");
    }

    handleActions(action) {
        switch (action.type) {
            case "RECEIVE_COMPANY_SIZE":
                this.company_sizes = action.company_sizes;
                this.emit("change");
                break;
            case "RECEIVE_INDUSTRIES":
                this.industries = action.industries;
                this.emit("change");
                break;
            case "CREATE_SIGNUP_FULFILLED":
                this.user = action.user
                this.emit("change");
        }
    }

}

const signupStore = new SignupStore;

dispatcher.register(signupStore.handleActions.bind(signupStore));


export default signupStore;