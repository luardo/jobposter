/**
 * Created by luardo on 11/11/16.
 */
import { combineReducers } from "redux"

import common from "./commonReducers"
import channels from "./channelsReducer"
import jobs from "./jobsReducer"
import login from "./loginReducer"
import location from "./locationReducer"
import order from "./orderReducer"
import signup from "./signupReducer"
import payment from "./paymentReducer"

export default combineReducers({
    channels,
    common,
    jobs,
    login,
    location,
    order,
    payment,
    signup
})