export default function reducer(state = {
    user: [],
    logged: false,
    fetched: false,
    fetching: false,
    error: null,
}, action) {
    switch (action.type) {

        case "CREATE_ACCOUNT_FULFILLED": {
            return {
                ...state,
                fetched: false,
                logged: true,
                user: action.payload,
            }
        }
    }
    return state
}