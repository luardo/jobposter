
export default function reducer(state = {
    channels: [],
    fetched: false,
    fetching: false,
    error: null,
}, action) {

    switch (action.type) {
        case "FETCH_CHANNELS": {
            return {...state, fetched: true, fetching: true }
        }

        case "FETCH_CHANNELS_REJECTED": {
            return {...state, fetched: false, error: action.payload, fetching:false }
        }

        case "FETCH_CHANNELS_FULFILLED": {
            return {
                ...state,
                fetched: true,
                fetching: false,
                channels: action.payload,
            }
        }
    }

    return state

}