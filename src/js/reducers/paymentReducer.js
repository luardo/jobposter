export default function reducer(state = {
  payment: [],
  status: 'INCOMPLETE',
  error: null,
}, action) {
  switch (action.type) {

    case "CREATE_PAYMENT_FULFILLED": {
      return {
        ...state,
        status: 'PENDING',
        payment: action.payload,
      }
    }
    case "CREATE_PAYMENT_REJECTED": {
      return {
        ...state,
        status: 'REJECTED',
      }
    }

    case "FETCH_PAYMENT_FULFILLED": {
      return {
        ...state,
        payment: action.payload
      }
    }
  }
  return state
}