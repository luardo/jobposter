export default function reducer(state = {
    session: [],
    logged: false,
    fetched: false,
    error: null,
}, action) {

    switch (action.type) {
        case "FETCH_SESSION": {
            return {...state, fetched: true }
        }
        case "FETCH_SESSION_REJECTED": {
            return {...state, fetched: false, error: action.payload }
        }
        case "FETCH_SESSION_FULFILLED": {
            return {
                ...state,
                logged: true,
                fetched: true,
                session: action.payload,
            }
        }
        case "REMOVE_SESSION_FULFILLED": {
            return {
                ...state,
                logged: false,
                fetched: true,
                session: [],
            }
        }
    }

    return state

}