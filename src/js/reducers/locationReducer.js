export default function reducer(state = {
    locations: [],
    location: [],
    logged: false,
    locationSet: false,
    fetched: false,
    fetching: false,
    error: null,
}, action) {
    switch (action.type) {
        case "FETCH_LOCATIONS": {
            return {...state, fetching: true }
        }
        case "FETCH_LOCATIONS_REJECTED": {
            return {...state, fetched: false, error: action.payload }
        }
        case "FETCH_LOCATIONS_FULFILLED": {
            return {
                ...state,
                fetched: true,
                locations: action.payload,
            }
        }
        case "SAVE_LOCATIONS_REJECTED": {
            return {
                ...state,
                fetched: false,
            }
        }
        case "SAVE_LOCATIONS_FULFILLED": {
            return {
                ...state,
                fetched: true,
                locationSet: true,
                location: action.payload,
            }
        }

    }
    return state
}