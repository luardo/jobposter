export default function reducer(state = {
    company: [],
    fetched: false,
    fetching: false,
    error: null,
}, action) {

    switch (action.type) {
        case "PATCH_COMPANY_REJECTED": {
            return {...state, fetched: false, error: action.payload, fetching:false }
        }
        case "PATCH_COMPANY_FULFILLED": {
            return {
                ...state,
                fetched: true,
                fetching: false,
                company: action.payload,
            }
        }
    }
    return state
}