export default function reducer(state = {
  logged: false,
  fetched: false,
  error: null,
  payment: [],
  channel_order: [],
  company: [],
  channels_fetched: false,
  orderParams: {
      job_id: '',
      channel_ids: [
          "44e0415a-86c2-43dc-ac7c-28b2c764a75a"
      ]
  },

}, action) {

  switch (action.type) {
      case "ADDED_TO_CART": {
          return {
              ...state,
              orderParams: action.payload
          }
      }
      case "CREATE_ORDER_FULFILLED": {
          return {
              ...state,
              order: action.payload
          }
      }
      case "UPDATE_ORDER_FULFILLED": {
          return {
              ...state,
              order: action.payload,
              channels_fetched: false
          }
      }
      case "FETCH_ORDER_FULFILLED": {
          return {
              ...state,
              order: action.payload
          }
      }
      case "FETCH_CHANNELS_ORDER_FULFILLED": {
          return {
              ...state,
              channel_order: action.payload,
              channels_fetched: true
          }
      }
      case "FETCH_ORDERS_FULFILLED": {
          return {
              ...state,
              ordersList: action.payload
          }
      }
              
      case "FETCH_COMPANY_FULFILLED": {
          return {
              ...state,
              fetched: true,
              company: action.payload,
          }
      }
  }

  return state
}