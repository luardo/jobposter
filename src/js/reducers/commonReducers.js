export default function reducer(state = {
  classifications: [],
  channels: [],
  schedules: [],
  seniorities: [],
  industries: [],
  occupation_list: [],
  years_of_experience: [],
  categories: [],
  fetching: false,
  fetched: false,
  error: null,
}, action) {

  switch (action.type) {
    case "FETCH_CLASSIFICATIONS_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_CLASSIFICATIONS_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_CLASSIFICATIONS_FULFILLED": {
      return {
        ...state,
        fetched: false,
        classifications: action.payload,
      }
    }

    case "FETCH_CHANNELS_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_CHANNELS_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_CHANNELS_FULFILLED": {
      return {
        ...state,
        fetched: false,
        channels: action.payload,
      }
    }

    case "FETCH_SCHEDULES_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_SCHEDULES_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_SCHEDULES_FULFILLED": {
      return {
        ...state,
        fetched: false,
        schedules: action.payload,
      }
    }

    case "FETCH_OCCUPATIONS_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_OCCUPATIONS_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_OCCUPATIONS_FULFILLED": {
      return {
        ...state,
        fetched: false,
        occupation_list: action.payload,
      }
    }

    case "FETCH_CATEGORIES_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_CATEGORIES_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_CATEGORIES_FULFILLED": {
      return {
        ...state,
        fetched: false,
        categories: action.payload,
      }
    }

    case "FETCH_SENIORITIES_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_SENIORITIES_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_SENIORITIES_FULFILLED": {
      return {
        ...state,
        fetched: false,
        seniorities: action.payload,
      }
    }

    case "FETCH_YEARS_OF_EXPERIENCE_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_YEARS_OF_EXPERIENCE_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_YEARS_OF_EXPERIENCE_FULFILLED": {
      return {
        ...state,
        fetched: false,
        years_of_experience: action.payload,
      }
    }

    case "FETCH_INDUSTRIES_PENDING": {
      return {...state, fetching: true}
    }
    case "FETCH_INDUSTRIES_REJECTED": {
      return {...state, fetched: false, error: action.payload}
    }
    case "FETCH_INDUSTRIES_FULFILLED": {
      return {
        ...state,
        fetched: false,
        industries: action.payload,
      }
    }


  }

  return state

}