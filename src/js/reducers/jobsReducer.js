export default function reducer(state = {
    jobs: [],
    job: [],
    jobDescriptions: [],
    logged: false,
    fetched: false,
    error: null,
    saved: false,
}, action) {

    switch (action.type) {
        case "FETCH_JOBS": {
            return {...state, fetched: true }
        }

        case "FETCH_JOBS_REJECTED": {
            return {...state, fetched: false, error: action.payload }
        }

        case "FETCH_JOBS_FULFILLED": {
            return {
                ...state,
                logged: true,
                fetched: true,
                jobs: action.payload,
            }
        }

        case "CREATE_JOBS_FULFILLED": {
            return {
                ...state,
                logged: true,
                fetched: true,
                saved:true,
                job: action.payload,
            }
        }

        case "FETCH_JOB_BY_ID_FULFILLED": {
            return {
                ...state,
                logged: true,
                fetched: true,
                job: action.payload,
            }
        }

        case "FETCH_JOBDESCRIPTION_REJECTED": {
            return {...state, fetched: false, error: action.payload }
        }

        case "FETCH_JOBDESCRIPTION_FULFILLED": {
            return {
                ...state,
                fetched: true,
                jobDescriptions: action.payload,
            }
        }
    }

    return state

}