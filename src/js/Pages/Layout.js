/**
 * Created by luardo on 29/10/16.
 */

import React from "react"
import { IndexLink, Link } from "react-router";
import { Nav, NavItem } from 'react-bootstrap';

import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';

export default class Layout extends React.Component {

    render() {
        return (

            <div>

                <header id="topnav">
                    <div className="topbar-main">
                        <div className="container">

                            <div className="topbar-left">
                                <a href="index.html" className="logo">
                                    <i className="zmdi zmdi-accounts-list-alt"></i>
                                    <span>Hire.One</span>
                                </a>
                            </div>

                            <div className="menu-extras">

                                <ul className="nav navbar-nav pull-right">

                                    <li className="nav-item">
                                        <a className="navbar-toggle">
                                            <div className="lines">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </li>



                                </ul>

                            </div>
                            <div className="clearfix"></div>

                        </div>

                    </div>



                </header>

                <div className="wrapper register">
                    <div className="container">

                     {this.props.children}

                    </div>
                </div>






                <div className="container">

                    <Nav bsStyle="pills" activeKey={1} >
                        <LinkContainer to={{ pathname: '/dashboard' }}>
                            <NavItem eventKey={1} href="/dashboard">Dashboard</NavItem>
                        </LinkContainer>
                        <LinkContainer to={{ pathname: '/jobs' }}>
                            <NavItem eventKey={2} href="/jobs">Manage jobs</NavItem>
                        </LinkContainer>
                        <LinkContainer to={{ pathname: '/jobs/add' }}>
                            <NavItem eventKey={3} href="/jobs/add">Publish a new position</NavItem>
                        </LinkContainer>
                        <LinkContainer to={{ pathname: '/jobboards' }}>
                            <NavItem eventKey={4} href="/jobboards">Job Boards</NavItem>
                        </LinkContainer>
                        <LinkContainer to={{ pathname: '/orders' }}>
                            <NavItem eventKey={5} href="/orders">Orders</NavItem>
                        </LinkContainer>

                        <LinkContainer to={{ pathname: '/login' }}>
                            <NavItem eventKey={6} href="/login">Login</NavItem>
                        </LinkContainer>

                        <LinkContainer to={{ pathname: '/logout' }}>
                            <NavItem eventKey={6} href="/logout">Logout</NavItem>
                        </LinkContainer>
                    </Nav>

                </div>
            </div>
        );

    }
}