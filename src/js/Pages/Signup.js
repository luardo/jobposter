/**
 * Created by luardo on 24/10/16.
 */
import React from "react";
import { bindActionCreators } from 'redux'
import { connect } from "react-redux"
import { createAccount } from "../actions/SignupActions";
import CreateAccountForm from "../components/Forms/CreateAccountForm"
import LocationPage from "./LocationPage"
import PageTitle from "../components/PageTitle"
import { fetchSession, isSessionSet, setSession } from "../actions/loginActions";

class Signup extends React.Component {
    constructor() {
        super();
        this.change = this.change.bind(this);
        this.state = {
            signup: {
                salutation: 'mr',
                first_name: '',
                last_name: '',
                email: '',
                company_name: '',
                password: '1q2w3e4r',
                password_confirmation: '1q2w3e4r'    
            },
        }
    }

    componentWillMount() {
        this.props.isSessionSet();
    }

    componentWillReceiveProps(nextProps) {
        const {session} = this.props;
        if (nextProps.user) {
            var authData = {
                "auth_token": nextProps.user.authentication_token,
                "user_id": nextProps.user.id
            }

            if (!nextProps.session.session.auth_token) {
                this.props.setSession(authData);
            }
        }
    }

    createSignUp() {
        const {signup} = this.state;
        this.props.createAccount(signup);
    }

    handleChange(field, value) {
        const {signup} = this.state;
        signup[field] = value;
        this.setState(signup);
    }

    change(field, event) {
        this.handleChange(field, event.target.value)
    }

    render() {
        const {signup} = this.state;
        const {session} = this.props;
        const {user} = this.props;

        if (session.logged == true) { //if user is created loads location form
            return (
                <div className="well bs-Component">
                    <PageTitle title="Select a job board" currentStep={1}/>
                    <LocationPage />
                </div>
            );
        }
        else {
            return (
                <div className="well bs-component">
                    <PageTitle title="Select a job board" currentStep={1}/>
                    <CreateAccountForm
                        signup={signup}
                        value={this.state.value}
                        onChange={this.change}
                        onHandleChange={this.handleChange.bind(this)}
                        onCreateSignup={this.createSignUp.bind(this)}
                        />
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        user: state.signup.user.user,
        session: state.login
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createAccount: bindActionCreators(createAccount, dispatch),
        fetchSession: bindActionCreators(fetchSession, dispatch),
        isSessionSet: bindActionCreators(fetchSession, dispatch),
        setSession: bindActionCreators(setSession, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup)