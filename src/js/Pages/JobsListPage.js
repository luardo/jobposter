import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import JobsList from '../components/JobsList'
import { fetchJobs } from '../actions/jobsActions'
import { getUserToken } from '../actions/loginActions'

import { FormGroup, Panel, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col, ListGroup, ListGroupItem } from 'react-bootstrap';



class JobsListPage extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <JobsList />
        );
    }
}

function mapStateToProps(state) {
    return {
        jobs: state.jobs.jobs,
        session: state.login.session
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchJobs: bindActionCreators(fetchJobs, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JobsListPage)