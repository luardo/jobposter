import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchOrders } from '../actions/orderActions'
import { getUserToken } from '../actions/loginActions'

import OrderListChannels from '../components/OrderListChannels'

import { IndexLink, Link } from "react-router"
import { Grid, Row, Table, Thumbnail, Button, Col, Glyphicon } from 'react-bootstrap'

class OrdersListPage extends React.Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.fetchOrders(this.props.getUserToken());
    }


    render() {
        const {ordersList} = this.props;
        if (ordersList) {
            const orderListElement = ordersList.orders.map(function (order) {
                return <tr key={order.id}>
                    <td>{order.id}</td>
                    <td>{new Date(order.updated_at).toString()}</td>
                    <td>{order.state}</td>
                    <td>{order.total}</td>
                    <td><Link to={'/checkout/payment/order/' + order.id}>See order</Link></td>
                </tr>

            })

            return (
                <ul>
                    <Table striped bordered condensed hover>
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Summe</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            {orderListElement}
                        </tbody>
                    </Table>

                </ul>
            )
        } else {
            return (
                <h3>no orders yet </h3>
            )
        }

    }
}

function mapStateToProps(state) {
    return {
        ordersList: state.order.ordersList,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchOrders: bindActionCreators(fetchOrders, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersListPage)