/**
 * Created by luardo on 01/11/16.
 */
import React from "react";
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import PageTitle from "../components/PageTitle"
import {getUserToken} from '../actions/loginActions'
import {fetchPayment} from '../actions/paymentActions'

import {confirmOrder, fetchOrderById} from '../actions/orderActions'
import {Row} from 'react-bootstrap'
import OrderTotals from '../components/Order/OrderTotals'

import OrderReviewSidebar from "../components/OrderReviewSidebar"

class ConfirmOrderPage extends React.Component {

  componentWillMount() {
    const {payment} = this.props;
    const OrderParamId = this.props.params.orderId;
    this.props.fetchPayment(OrderParamId, this.props.getUserToken());
    this.props.disableNavigation(); //disable navigation menu for this page

  }

  componentWillUnmount() {
    this.props.resetNavigationVisibility(); //disable navigation menu for this page
  }

  confirmOrder() {
    const {getUserToken} = this.props,
      {orderId} = this.props.params;
    this.props.confirmOrder(orderId, getUserToken());
  }

  onRemoveItem(param) {
    return false;
  }

  channelsInCart(order) {
    if (order) {
      return order.channel_ids;
    } else {
      return null;
    }
  }

  render() {
    const {fetch_channels, order, orderReducer} = this.props;
    if (order) {
      if (order.state == "confirmed") {
        return (
          <div>
            <PageTitle title="Confirm Order" currentStep={4}/>
            <Row>
              <div className="col-sm-12 col-xl-9">
                <div className="card-box">
                  <Row>
                    <h5>Your Order Has been confirmed</h5>
                    <p>Our publisher partners might take a couple days for approve your order. We will notify you when
                      the job offer is published.</p>

                  </Row>
                </div>
              </div>
              <div className="col-sm-12 col-xl-3">

              </div>
            </Row>

          </div>

        )
      } else if (order.state == "new") {
        return (
          <div>
            <PageTitle title="Confirm Order" currentStep={4}/>
            <Row>
              <div className="col-sm-12 col-xl-9">
                <div className="card-box">
                  <Row>
                    <h5>Thank you for using our application</h5>
                    <p>Our publisher partners might take a couple days for approve your order. We will notify you when
                      the job offer is published.</p>
                  </Row>
                </div>
              </div>
              <div className="col-sm-12 col-xl-3">
              </div>
            </Row>
          </div>
        )
      }
      else {
        return (
          <div>
            <PageTitle title="Confirm Order" currentStep={4}/>
            <Row>
              <div className="col-sm-12 col-xl-9">
                <div className="card-box">
                  <Row>
                    <OrderTotals order={order}/>
                    <a href="#"
                       onClick={() => this.confirmOrder(order)}
                       className="btn btn-block btn-lg btn-info waves-effect waves-light">Confirm</a>
                    <br />

                  </Row>
                </div>
              </div>
              <div className="col-sm-12 col-xl-3">
                <OrderReviewSidebar
                  order={order}
                  buttonText="Confirm Order"
                  fetch_channels={fetch_channels}
                  channelIds={this.channelsInCart(order)}
                  orderAttr={orderReducer}
                  onChangeHandler={this.onRemoveItem.bind(this)}/>
              </div>
            </Row>

          </div>
        );
      }

    } else {
      return null;
    }

  }
}

function mapStateToProps(state) {
  return {
    order: state.order.order.order,
    channel_order: state.order.channel_order,
    channels_fetched: state.order.channels_fetched,
    fetch_channels: state.order.fetch_channels,
    company: state.order.company,
    company_fetched: state.order.fetched,
    fetched: state.location.fetched,
    locations: state.location.locations,
    orderReducer: state.order,
    payment: state.payment,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchOrderById: bindActionCreators(fetchOrderById, dispatch),
    getUserToken: bindActionCreators(getUserToken, dispatch),
    confirmOrder: bindActionCreators(confirmOrder, dispatch),
    fetchPayment: bindActionCreators(fetchPayment, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmOrderPage);