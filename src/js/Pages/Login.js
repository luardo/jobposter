/**
 * Created by luardo on 03/11/16.
 */
import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchSession, isSessionSet } from "../actions/loginActions";
import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col } from 'react-bootstrap';
import { Router, browserHistory, Link } from 'react-router';


class Login extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            "session": [],
            "authData": {
                "email": '',
                "password": ''
            }
        }
    }

    componentWillMount() {
        this.props.isSessionSet();

    }

    componentWillUnmount() {
        this.props.isSessionSet();
    }

    onChange(field, event) {
        this.handleChange(field, event.target.value)
    }

    handleChange(field, value) {
        const {authData} = this.state;
        authData[field] = value;
        this.setState(authData);
    }

    handleSubmit(e) {
        //we don't want the form to submit, so we prevent the default behavior
        e.preventDefault();
        const {authData} = this.state;
        this.props.fetchSession(authData);
        browserHistory.push('/jobs/add');
    }

    render() {
        const {authData} = this.state;
        const {session} = this.props;
        const {auth_token} = this.props;

        if (this.props.logged == false) {
            return (
                <div className="well bs-component sss">
                    {session.auth_token}
                    <form onSubmit={this.handleSubmit}>
                        <FormGroup controlId="email">
                            <ControlLabel>email</ControlLabel>
                            <FormControl type="email" value={authData.email} placeholder="email"
                                onChange={this.onChange.bind(this, 'email')} />
                            <FormControl.Feedback />
                        </FormGroup>

                        <FormGroup controlId="password">
                            <ControlLabel>Password</ControlLabel>
                            <FormControl type="password" value={authData.password} placeholder="password"
                                onChange={this.onChange.bind(this, 'password')} />
                            <FormControl.Feedback />
                        </FormGroup>

                        <Button bsStyle="success" type="submit">Login </Button>

                        <div className="signup-new-user"><Link to="/signup" >Create an account</Link></div>
                    </form>
                </div>

            );
        } else {
            return (
                <h3>signed in</h3>
            );
            browserHistory.push('/signup');
        }
    }

};

function mapStateToProps(state) {
    return {
        session: state.login.session,
        logged: state.login.logged
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchSession: bindActionCreators(fetchSession, dispatch),
        isSessionSet: bindActionCreators(isSessionSet, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Login)
