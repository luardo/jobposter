import React from "react"

import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import { fetchChannels } from "../actions/channelsActions"

import JobboardForm from "../components/JobboardForm"
import { getUserToken } from '../actions/loginActions'
import { addToBasket, createOrder } from '../actions/orderActions'

import { Grid, Row, Thumbnail, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col } from 'react-bootstrap';
import { concat, sortBy, map, includes, add, union, contains, remove } from 'lodash'

class ReviewOrderPage extends React.Component {
    
    constructor() {
        super();
    }

    render() {
        
    }
}

const mapStateToProps = (state) => {
    return {
        order: state.order.order
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createOrder: bindActionCreators(createOrder, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewOrderPage);
