/**
 * Created by luardo on 30/10/16.
 */
import React from "react";
import axios from "axios";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { fetchLocations, saveLocation } from "../actions/locationActions"
import { isSessionSet } from "../actions/loginActions"
import { updateCompany } from "../actions/companyActions"

import LocationsList from "../components/LocationsList"
import PageTitle from "../components/PageTitle"
import Industries from "../components/Industries"
import CompanySizes from "../components/CompanySizes"

import * as UserActions from "../actions/UserActions"

import { IndexLink, Link, browserHistory } from "react-router"

import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col, ListGroup, ListGroupItem } from 'react-bootstrap';

class LocationPage extends React.Component {

    constructor(props) {
        super(props);
        this.getLocations = this.getLocations.bind(this);

        this.state = {
            location: {
                name: 'Main Office',
                street: '',
                postal_code: '',
                city: '',
                region: '',
                country: 'Germany'
            },
            locations: [],
            company: {
                industry: '',
                company_size: ''
            }
        }
    }

    componentWillMount() {
        this.props.isSessionSet();
        this.props.fetchLocations(this.props.session.auth_token);

    }

    componentWillReceiveProps(nextProps) {
        const {locationSet} = nextProps;
        if (locationSet == true) {
            browserHistory.push('/jobs/add');
        }
    }


    getLocations() {
        const {session} = this.props;
        this.props.fetchLocations(session.auth_token);
    }


    postLocation() {
        const {location} = this.state;
        const {session} = this.props;

        var auth_token = '';

        if (session.auth_token) {
            auth_token = session.auth_token;
        }

        this.props.saveLocation(location, auth_token);
       // this.props.updateCompany(session.user_id, location, auth_token);

    }

    onChange(field, event) {
        this.handleChange(field, event.target.value)
    }

    handleChange(field, value) {
        const {location} = this.state;
        location[field] = value;
        this.setState(location);
    }

    updateCompanyProfile(field, value) {
        const {locationSet} = this.props;
        const {session} = this.props;
        const {company} = this.state;
        company[field] = value;
        this.props.updateCompany(session.user_id, company, session.auth_token);

    }

    render() {
        const {session} = this.props;
        const {location} = this.state;
        const {locations} = this.props;
        const {locationSet} = this.props;
        var auth_token = '';


        if (session.auth_token) {
            auth_token = session.auth_token;
        } else {
            browserHistory.push('/signup');

        }
        if (locationSet == false) {
            return (
                <form>
                    <PageTitle title="Complete your company details " />


                    <div className="row">
                        <div className="col-sm-12 col-xl-9 col-xl-offset-2">
                            <div className="card-box">
                                <div className="row">
                                    <div className="col-lg-12 col-sm-12 col-xs-12 col-md-12 ">

                                        <div className="row">
                                            <Col xs={12} md={6}>

                                                <FormGroup controlId="name">
                                                    <FormControl type="hidden" value="Main Office" placeholder="Enter Street"
                                                        onChange={this.onChange.bind(this, 'name')} />
                                                    <FormControl.Feedback />
                                                </FormGroup>

                                                <FormGroup controlId="street">
                                                    <ControlLabel>Street and Number</ControlLabel>
                                                    <FormControl type="text" value={location.street} placeholder="Enter first name"
                                                        onChange={this.onChange.bind(this, 'street')} />
                                                    <FormControl.Feedback />
                                                </FormGroup>

                                                <div className="row">
                                                    <Col xs={6}>
                                                        <FormGroup controlId="postal_code">
                                                            <ControlLabel>Postal Code</ControlLabel>
                                                            <FormControl type="text" value={location.postal_code} placeholder="Enter your last name"
                                                                onChange={this.onChange.bind(this, 'postal_code')} />
                                                            <FormControl.Feedback />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs={6}>
                                                        <FormGroup controlId="city">
                                                            <ControlLabel>City</ControlLabel>
                                                            <FormControl type="email" value={location.city} placeholder="Enter your email"
                                                                onChange={this.onChange.bind(this, 'city')} />
                                                            <FormControl.Feedback />
                                                        </FormGroup>
                                                    </Col>

                                                </div>

                                            </Col>

                                            <Col xs={12} md={6}>
                                                <Industries onHandleChange={this.updateCompanyProfile.bind(this)} />
                                                <CompanySizes onHandleChange={this.updateCompanyProfile.bind(this)} />

                                            </Col>

                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12  text-xs-center">
                                                <br />
                                                <Button bsClass="btn btn-primary waves-effect waves-light btn-lg" onClick={this.postLocation.bind(this)}>Save location</Button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                </form>
            );
        } else {
            return null;
        }


    }
}

function mapStateToProps(state) {
    return {
        locations: state.location.locations,
        locationSet: state.location.locationSet,
        session: state.login.session
    }
}

function mapDispatchToProps(dispatch) {
    return {
        isSessionSet: bindActionCreators(isSessionSet, dispatch),
        fetchLocations: bindActionCreators(fetchLocations, dispatch),
        saveLocation: bindActionCreators(saveLocation, dispatch),
        updateCompany: bindActionCreators(updateCompany, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(LocationPage)