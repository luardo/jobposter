import React from "react"

import { bindActionCreators } from "redux"
import { connect } from "react-redux"


import PaymentForm from "../components/Forms/PaymentForm"
import { addToBasket, createOrder } from '../actions/orderActions'
import { createPayment } from '../actions/paymentActions'
import { getUserToken } from '../actions/loginActions'

import { FormGroup, FormControl, ControlLabel, FieldGroup, Radio, Checkbox, Button } from 'react-bootstrap';
import { concat, sortBy, map, includes, add, union, contains, remove } from 'lodash'

class PaymentPage extends React.Component {

    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
        this.state = {
            paymentParams: {
                order_id: props.params.orderId,
                street: '',
                city: '',
                postal_code: '',
                country: ''
            }
        }
    }



    handleChange(field, value) {
        const {paymentParams} = this.state;
        paymentParams[field] = value;
        this.setState(paymentParams);
    }

    change(field, event) {
        this.handleChange(field, event.target.value)
    }

    submitPayment() {
        const {paymentParams} = this.state;
        this.props.createPayment(paymentParams, this.props.getUserToken);
    }

    render() {
        const {paymentParams} = this.state;

        if (paymentParams) {
            return (
                <div>
                    <PaymentForm
                        paymentData={paymentParams}
                        onChange={this.change.bind(this)}
                        />
                    <Button bsSize="large" bsStyle="primary" onClick={this.submitPayment.bind(this)}>Submit</Button>

                </div>
            );
        }

    }
}

function mapStateToProps(state) {
    return {
        channels: state.channels.channels,
        orderParams: state.order.orderParams,
        order: state.order.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createOrder: bindActionCreators(createOrder, dispatch),
        createPayment: bindActionCreators(createPayment, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPage);
