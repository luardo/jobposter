import React from "react"
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import JobsList from '../components/JobsList'
import {fetchJobs} from '../actions/jobsActions'
import {getUserToken} from '../actions/loginActions'


require("../scss/table.scss");

class DashboardPage extends React.Component {

  constructor(props) {
    super(props);

  }

  render() {

    return (

      <div>
        <div className="container">

          <div className="col-lg-12 col-md-12 col-sm-8">
            <JobsList />
          </div>
        </div>
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    jobs: state.jobs.jobs,
    session: state.login.session
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchJobs: bindActionCreators(fetchJobs, dispatch),
    getUserToken: bindActionCreators(getUserToken, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage)