import React from "react"

import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import { fetchChannels, filterChannels } from "../actions/channelsActions"

import JobBoardChannels from "../components/JobBoardChannels"
import PageTitle from "../components/PageTitle"
import OrderReviewSidebar from "../components/OrderReviewSidebar"
import { getUserToken } from '../actions/loginActions'
import { addToBasket, createOrder, hasOrder, fetchOrderById, updateOrder } from '../actions/orderActions'

import { IndexLink, Link, browserHistory } from "react-router";
import { Grid, Row, Thumbnail, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col, Pagination } from 'react-bootstrap'
import { concat, sortBy, filter, map, includes, add, union, contains, remove, pull } from 'lodash'

require("../scss/channelList.scss");
require("../scss/pagination.scss");

class JobboardPage extends React.Component {

    constructor() {
        super();
        this.isSelected = this.isSelected.bind(this);
        this.saveOrderLocalStorage = this.saveOrderLocalStorage.bind(this);
        this.filterChannels = this.filterChannels.bind(this);

        this.state = {
            orderParams: {
                job_id: '',
                channel_ids: []
            },
            activePage: 1,
            totalChannels: 0,
            filter: 'list_price'
        }
    }

    componentWillMount() {
        this.props.fetchChannels(this.props.getUserToken());
        const {orderParams} = this.state;

        let order = this.props.hasOrder(); //if order exist, loads order
        if (order) {
            this.props.fetchOrderById(order.order_id, this.props.getUserToken());
        }

        this.setState({
            orderParams: {
                job_id: this.props.params.jobId
            }
        });
    }

    onRemoveItem(channelid) {
        var {orderParams} = this.state;
        var channels = orderParams.channel_ids;
        var isIncluded = _.includes(channels, channelid);
        _.pull(channels, channelid);

        orderParams['channel_ids'] = channels

        this.setState({
            orderParams: orderParams
        });
        if (this.props.order) { //if order has been created it PATCH the current order
            this.props.updateOrder(this.props.order.order.id, orderParams, this.props.getUserToken());
        }
    }

    onChangeHandler(channelid) {
        var {orderParams} = this.state;
        var channels = orderParams.channel_ids;

        if (this.isSelected.bind(this, channelid) == true) {
            channels = _.pull(channels, channelid);
        } else {
            channels = _.union(channels, [channelid]);
        }

        orderParams['channel_ids'] = channels

        this.setState({
            orderParams: orderParams
        });

        if (this.props.order) { //if order has been created it PATCH the current order
            this.props.updateOrder(this.props.order.order.id, orderParams, this.props.getUserToken());
        } else {
            this.props.createOrder(orderParams, this.props.getUserToken()); // creates a new order with the channel id
        }
    }

    isSelected(channelId) {
        const {orderParams} = this.state;
        var isIncluded = _.includes(orderParams.channel_ids, channelId);
        return isIncluded;
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.order) {
            var {orderParams} = this.state;
            orderParams['channel_ids'] = nextProps.order.order.channel_ids

            this.setState({
                orderParams: orderParams
            });

            this.saveOrderLocalStorage(nextProps.order.order.id); //saves the session in local storage
        }
    }

    saveOrderLocalStorage(order_id) {
        var order = {
            job_id: this.props.params.jobId,
            order_id: order_id
        }
        localStorage.order = JSON.stringify(order);
    }

    onClickPublish() {
        const {orderParams} = this.props;
        this.props.createOrder(orderParams, this.props.getUserToken());
    }

    //handles page change
    changePage(eventKey) {
        this.setState({
            activePage: eventKey
        });
    }

    filterChannels(channels, type) {
        this.props.filterChannels(channels, type);
    }

    render() {
        const {channels, order, orderParams} = this.props;
        const self = this;
        const {activePage} = this.state;

        if (channels) {

            return (
                <div>
                    <PageTitle title="Select a job board" />
                    <Row>
                        <div className="col-sm-12 col-xl-9">
                            <div className="card-box">

                                <Pagination
                                    bsSize="large"
                                    prev={true}
                                    next={true}
                                    items={Math.ceil(channels.length / 8)}
                                    activePage={this.state.activePage}
                                    maxButtons={4}
                                    boundaryLinks={true}
                                    ellipsis={true}
                                    first={true}
                                    last={true}
                                    onSelect={this.changePage.bind(this)} />

                                <Row>
                                    <div className="col-lg-3 col-sm-3 col-xs-3 col-md-6">
                                        <p>
                                            <input type="email" className="form-control" id="exampleInputEmail1" placeholder="Search ..." />
                                        </p>
                                    </div>

                                    <div className="col-lg-6 col-sm-6 col-xs-6 col-md-6">
                                        <nav className="nav nav-inline">
                                            <a className="nav-link active" href="#">Sort by:</a>
                                            <a className="nav-link" href="#">Price</a>
                                            <a className="nav-link" href="#">Name</a>
                                            <Link className="nav-link"
                                                onClick={this.filterChannels.bind(this, channels, 'premium')}>Show Premium
                                            </Link>
                                            <Link className="nav-link"
                                                onClick={this.filterChannels.bind(this, channels, 'free')}>Show free 
                                            </Link>
                                        </nav>
                                    </div>


                                </Row>
                                <Row>
                                    <JobBoardChannels channelsArray={channels} resultsPerPage={8}
                                        activePage={activePage}
                                        isSelected={this.isSelected.bind(this)}
                                        onChangeHandler={this.onChangeHandler.bind(this)}
                                        />
                                </Row>
                                <Pagination
                                    bsSize="large"
                                    items={channels.length / 8}
                                    activePage={this.state.activePage}
                                    onSelect={this.changePage.bind(this)} />
                            </div>
                        </div>
                        <div className="col-sm-12 col-xl-3">
                            <OrderReviewSidebar onChangeHandler={self.onRemoveItem.bind(self)} />
                        </div>
                    </Row>

                </div>
            );
        } else {
            return (
                <div>no jobboards</div>
            );
        }
    }
}


function mapStateToProps(state) {
    return {
        channels: state.channels.channels,
        orderParams: state.order.orderParams,
        order: state.order.order
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addToBasket: bindActionCreators(addToBasket, dispatch),
        createOrder: bindActionCreators(createOrder, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch),
        fetchChannels: bindActionCreators(fetchChannels, dispatch),
        fetchOrderById: bindActionCreators(fetchOrderById, dispatch),
        updateOrder: bindActionCreators(updateOrder, dispatch),
        hasOrder: bindActionCreators(hasOrder, dispatch),
        filterChannels: bindActionCreators(filterChannels, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JobboardPage);