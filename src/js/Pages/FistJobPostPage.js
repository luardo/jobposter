/**
 * Created by luardo on 01/11/16.
 */
import React from "react";

import { bindActionCreators } from 'redux'
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux'
import { createJob } from "../actions/jobsActions";
import CreateJobForm from "../components/Forms/CreateJobForm";
import SessionStore from "../Stores/SessionStore"
import { fetchClassifications, getCategories, getSchedules, getChannels } from "../actions/commonActions";
import { fetchLocations } from "../actions/locationActions";
import { fetchSession, isSessionSet } from "../actions/locationActions";

import { Router, browserHistory } from 'react-router';

class FirstJobPostPage extends React.Component {

    constructor() {
        super();

        this.getSession = SessionStore.getSession.bind(this);

        this.state = {
            'session': [],
            'job': {
                'position': '',
                'location_id': '',
                "descriptions_attributes": [
                    {
                        "description_type": "company",
                        "description": "Schöne Musik fängt mit Songs an, die auch jemand kennt. Wir von Songstar entwickeln cloudbasierte Musiksoftware, mit denen wir unsere Kunden im produzieren und im Vertrieb ihrer Songs unterstützen. Zur Ergänzung unseres Teams suchen wir zum nächstmöglichen Zeitpunkt eine/n"
                    },
                    {
                        "description_type": "job",
                        "description": "<ul><li>In Ihrer Position unterstützen Sie die Vertriebs- und Marketingleitung im täglichen Geschäftsablauf und übernehmen anfallende Bürotätigkeiten\n</li><li>Sie helfen den Teams aus Marketing und Vertrieb bei der Vorbereitung, Durchführung und Auswertung von Projekten\n</li><li>Als erster Ansprechpartner im allgemeinen Schriftverkehr sind Sie verantwortlich für die Koordination der Kommunikation\n</li><li>Selbstständige Marktrecherchen sowie die Pflege von Datenbanken\n</li><li>Vor allem aber gehen Sie mit Spaß und Leidenschaft Ihre Aufgaben an</li></ul>"
                    },
                    {
                        "description_type": "qualifications",
                        "description": "<ul><li>Erste Berufserfahrungen in ähnlicher Position sind definitiv von Vorteil\n</li><li>Sie wissen wie man auf unterschiedlichen Ebenen kommuniziert und verfügen über ein professionelles Auftreten\n</li><li>Routinierter Umgang mit gängigen Office-Anwendungen\n</li><li>Sie kennzeichnet ein hohes Maß an Eigeninitiative, Musik-Affinität und ein kreativer Kopf\n</li><li>Sehr gute Sprachkenntnisse in deutsch und englisch</li></ul>"
                    },
                    {
                        "description_type": "additional_information",
                        "description": "<ul><li>Die ideale Kombination aus Musik und Vertrieb\n</li><li>Flexible Arbeitszeiten, leistungsgerechte Entlohnung und Berlin als europäische Kreativ- und Technologiemetropole\n</li><li>Spielraum sich mit unserem Unternehmen zu entwickeln</li></ul>"
                    }
                ],
                'category': null,
                'occupation_list': null,
                'skill_list': ['php', 'javascript'],
                'apply_email': '',
                'apply_url': ''
            }
        }
    }

    handleChange(field, value) {
        const {job} = this.state;

        if (field == "descriptions_attributes") {
            var index = this.getPosInArray(value.description_type, job.descriptions_attributes, 'description_type');
            var descriptions_attributesArray = job.descriptions_attributes;

            if (index) {
                descriptions_attributesArray[index] = {
                    "description_type": value.description_type,
                    "description": value.description
                }

                job[field][index] = {
                    "description_type": value.description_type,
                    "description": value.description
                };
            }
        } else {
            job[field] = value;
        }

        this.setState(job);
    }

    getPosInArray(needle, array, node) {
        var length = array.length;
        for (var i = 0; i < length; i++) {
            if (array[i][node] == needle) return i;
        }
    }

    componentWillMount() {
        this.setState({
            session: SessionStore.getSession()
        });

        this.props.fetchLocations;
        this.props.getCategories;

    }

    getSession() {
        this.setState({
            session: SessionStore.getSession()
        });
    }

    componentDidMount() {
        document.title = "Create new Job opening";
    }

    componentWillUnmount() {
        SessionStore.removeListener("change", this.getSession);
    }


    change(field, event) {
        this.handleChange(field, event.target.value)
    }

    setUserId() {
        const {session} = this.state;
        const job = this.job;
        job.hiring_team_member_ids = session.user_id;
        this.setState({
            job: job
        });
    }

    submitCreateJob() {
        this.setUserId.bind(this);
        const {job} = this.state;
        const {session} = this.state;

        this.props.createJob(job, session.auth_token);
    }

    render() {
        const {job} = this.state;
        const {session} = this.state;

        if (session.auth_token) {
            return (
                <div>
                    <PageTitle title="Complete your company details " />

                    <div className="row">
                        <div className="col-sm-12 col-xl-6 col-xl-offset-3">
                            <div className="card-box">
                                <div className="row">
                                    <div className="col-lg-12 col-sm-12 col-xs-12 col-md-12 ">

                                        <CreateJobForm
                                            job={job}
                                            onChange={this.change.bind(this)}
                                            onHandleChange={this.handleChange.bind(this)}
                                            />
                                        <Button bsStyle="success" onClick={this.submitCreateJob.bind(this)}>Publish new job offer</Button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            browserHistory.push('/login');
        }
    }
}


function mapStateToProps(state) {
    return {
        session: state.login.session,
        jobs: state.jobs.saved
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createJob: bindActionCreators(createJob, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FirstJobPostPage)