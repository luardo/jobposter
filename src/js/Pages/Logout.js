import React from "react"

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { logout, isSessionSet } from '../actions/loginActions'
import { browserHistory } from 'react-router'

class Logout extends React.Component {

    constructor() {
        super();
    }


    componentWillMount() {
        const {session} = this.props;
        this.props.isSessionSet();
    }

    componentWillReceiveProps (nextProps) {
        if(nextProps.session.logged == true) {
            this.props.logout();
        }
    }
    
    render() {
        return (
            <h3>You are logged out</h3>
        )
    }
}


function mapStateToProps(state) {
    return {
        session: state.login,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: bindActionCreators(logout, dispatch),
        isSessionSet: bindActionCreators(isSessionSet, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)

