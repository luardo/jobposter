import axios from "axios"
import { browserHistory } from "react-router";

export function createPayment(payment, auth_token) {
    return function (dispatch) {
        axios.post(' https://api.gohiring.com/payments?auth_token=' + auth_token, {
            payment
        },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "CREATE_PAYMENT_FULFILLED", payload: response.data })
                browserHistory.push('/order/confirm/' + payment.order_id);

            })
            .catch((err) => {
                dispatch({ type: "CREATE_PAYMENT_REJECTED", payload: err })
            });
    }
}

export function fetchPayment(order_id, auth_token) {
    return function (dispatch) {
        axios.get(' https://api.gohiring.com/payments?order_id=' + order_id + '&auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_PAYMENT_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_PAYMENT_REJECTED", payload: err })
            });
    }
}

export function paymentExist(order_id, auth_token) {
    axios.get(' https://api.gohiring.com/payments?order_id=' + order_id + '&auth_token=' + auth_token,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then((response) => {
            if (response.data.payment.lenght > 0) {
                return true;
            } else {
                return false;
            }
        })
        .catch((err) => {
            console.log(err);
            return false;
        });
}