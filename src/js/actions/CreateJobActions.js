/**
 * Created by luardo on 01/11/16.
 */
import dispatcher from "../dispatcher"
import axios from "axios";

export function getClassifications() {

    axios.get('https://api.gohiring.com:443/classifications',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_CLASSIFICATIONS", classifications:
                response.data.classifications
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}

export function getChannels() {

    axios.get('https://api.gohiring.com:443/channels',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_CHANNELS", channels:
                response.data.channels
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}

export function getChannelById(id) {
    axios.get('https://api.gohiring.com:443/channels/' + id,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_CHANNEL", channel:
                response.data.channels
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}


//depreciated
export function getSchedules() {
    axios.get('https://api.gohiring.com:443/schedules',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_SCHEDULES", schedules:
                response.data.schedules
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}

export function getYearsOfExperience() {
    axios.get('https://api.gohiring.com:443/years_of_experiences',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_YEARS_OF_EXPERIENCE", years_of_experience:
                response.data.years_of_experience
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}

export function getOccupations(category_code) {

    let session = [];
    if (localStorage.session) {
        session = JSON.parse(localStorage.session);

      axios.get('https://api.gohiring.com:443/occupations?category_code=' + category_code + '&auth_token=' + session.auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then(function (response) {
                dispatcher.dispatch({
                    type: "FETCH_OCCUPATIONS", occupations:
                    response.data.occupations
                });
            })
            .catch(function (error) {
                console.log(error);
            });

    }


}

export function getSeniorities() {
    axios.get('https://api.gohiring.com:443/seniorities',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_SENIORITIES", seniorities:
                response.data.seniorities
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}

export function getCategories() {
    axios.get('https://api.gohiring.com:443/categories',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "FETCH_CATEGORIES", categories:
                response.data.categories
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}