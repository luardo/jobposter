import axios from "axios";

export function fetchSession(authData) {
    return function (dispatch) {
        if (authData) {
            axios.post(' https://api.gohiring.com:443/sessions?auth_token=gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr', {
                email: authData.email,
                password: authData.password
            }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/vnd.gohiring+json;version=v2',
                        'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                    }
                })
                .then((response) => {
                    localStorage.session = JSON.stringify(response.data);
                    dispatch({ type: "FETCH_SESSION_FULFILLED", payload: response.data })
                })
                .catch((err) => {
                    dispatch({ type: "FETCH_SESSION_REJECTED", payload: err })
                });
        }
    }
}

export function setSession(session) {
    return function (dispatch) {
        localStorage.session = JSON.stringify(session);
        dispatch({ type: "FETCH_SESSION_FULFILLED", payload: session })
    }

}

export function isSessionSet() {
    return function (dispatch) {
        if (localStorage.session) {
            dispatch({ type: "FETCH_SESSION_FULFILLED", payload: JSON.parse(localStorage.session) })
        } else {
            return false;
        }
    }
}

export function logout() {
    return function (dispatch) {
        if (localStorage.session) {
            localStorage.removeItem("session");
            localStorage.removeItem("order");
            dispatch({ type: "REMOVE_SESSION_FULFILLED", payload: [] })
        }
    }
}

export function getUserToken() {
    return function (dispatch) {
        if (localStorage.session) {
            var session = JSON.parse(localStorage.session);
            return session['auth_token'];
        } else {
            return false;
        }
    }
}