/**
 * Created by luardo on 30/10/16.
 */
import dispatcher from "../dispatcher"
import axios from "axios";

export function createUser(user) {
    dispatcher.dispatch({
        type: "CREATE_USER",
        user
    });
}


export function getCompanyById(id) {
    axios.get('https://api.gohiring.com:443/companies/' + id,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "RECEIVE_COMPANY_DATA", company:
                    response.data.company
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}

export function getUserById(id) {
    axios.get('https://api.gohiring.com:443/companies/' + id,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "RECEIVE_COMPANY_DATA", company:
                    response.data.company
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}


export function createLocation(locationData, auth_token) {
    var location = locationData;
    axios.post(' https://api.gohiring.com:443/locations?auth_token=' + auth_token, {
        location
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.gohiring+json;version=v2',
            'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
        }
    })
        .then(function (response) {
            dispatcher.dispatch({
                type: "CREATE_LOCATION", location:
                    response.data.locations
            });
        })
        .catch(function (error) {
            if (error.response) {
                console.log(error.response.data.errors);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
        });

    axios.get('https://api.gohiring.com:443/locations?auth_token=' + auth_token ,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "GET_LOCATIONS", locations:
                    response.data.locations
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}


export function loadLocations(auth_token) {
    axios.get('https://api.gohiring.com:443/locations?auth_token=' + auth_token ,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "GET_LOCATIONS", locations:
                    response.data.locations
            });

        })
        .catch(function (error) {
            console.log(error);
        });
}