import axios from "axios";

export function createJob(job, auth_token) {

    var session = [];
    if (!auth_token) {
        if (localStorage.session) {
            session = JSON.parse(localStorage.session);
            auth_token = session.auth_token;
        } else {
            return false;
        }
    }

    return function (dispatch) {
        axios.post(' https://api.gohiring.com:443/jobs?auth_token=' + auth_token, {
            job
        },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "CREATE_JOBS_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "CREATE_JOBS_REJECTED", payload: err })
            });
    }
}

export function fetchJobs(auth_token) {

    return function (dispatch) {
        axios.get(' https://api.gohiring.com:443/jobs?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_JOBS_FULFILLED", payload: response.data.jobs })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_JOBS_REJECTED", payload: err })
            });
    }
}

export function fetchJobById(job_id, auth_token) {

    return function (dispatch) {
        axios.get(' https://api.gohiring.com:443/jobs/' + job_id + '?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_JOB_BY_ID_FULFILLED", payload: response.data.job })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_JOB_BY_ID_REJECTED", payload: err })
            });
    }
}

export function fetchJobDescription(auth_token, ids) {

    var params = ids.map(function (id) {
        return "ids[]=" + id;
    });

    params = params.join('&');

    return function (dispatch) {
        axios.get(' https://api.gohiring.com:443/job_descriptions?' + params + '&auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_JOBDESCRIPTION_FULFILLED", payload: response.data.job_descriptions })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_JOBDESCRIPTION_REJECTED", payload: err })
            });
    }
}
