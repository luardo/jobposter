/**
 * Created by luardo on 29/10/16.
 */
import dispatcher from "../dispatcher"
import axios from "axios";

export function loadCompanySizes() {
    dispatcher.dispatch({ type: "FETCH_DATA" });
    axios.get('https://api.gohiring.com:443/company_sizes?auth_token=gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "RECEIVE_COMPANY_SIZE", company_sizes:
                response.data.company_sizes
            });

        })
        .catch(function (error) {
            console.log(error);
        });

}

export function createAccount(signup) {
    return function (dispatch) {
        axios.post(' https://api.gohiring.com:443/signups', {
            signup
        }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "CREATE_ACCOUNT_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "CREATE_ACCOUNT_REJECTED", payload: err })
            });
    }
}




export function loadIndustries() {
    axios.get('https://api.gohiring.com:443/industries?auth_token=gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr',
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/vnd.gohiring+json;version=v2',
                'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
            }
        })
        .then(function (response) {
            dispatcher.dispatch({
                type: "RECEIVE_INDUSTRIES", industries:
                response.data.industries
            });

        });

}
