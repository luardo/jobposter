import axios from "axios";
import { filter } from 'lodash'

export function fetchChannels(auth_token) {
    return function (dispatch) {
        dispatch({ type: "FETCH_CHANNELS", payload: { fetching: true } });
        axios.get(' https://api.gohiring.com:443/channels?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_CHANNELS_FULFILLED", payload: response.data.channels })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_CHANNELS_REJECTED", payload: err })
            });
    }
}

export function getChannel(id, auth_token) {
    return function (dispatch) {
        dispatch({ type: "FETCH_CHANNELS", payload: { fetching: true } });
        axios.get(' https://api.gohiring.com/channels/' + id + '?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_CHANNELS_FULFILLED", payload: response.data.channels })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_CHANNELS_REJECTED", payload: err })
            });
    }
}

export function getChannelAttribute(id, attr, auth_token) {
    return function (dispatch) {
        axios.get(' https://api.gohiring.com/channels/' + id + '?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                return response.data.channels[attr];
            })
            .catch((err) => {
                dispatch({ type: "FETCH_CHANNELS_REJECTED", payload: err })
            });
    }
}

export function filterChannels(channels, type) {
    return function (dispatch) {
        if (type == 'free') {
            channels = _.filter(channels, { 'list_price': '0.0' });
            dispatch({ type: "FETCH_CHANNELS_FULFILLED", payload: channels })
        } else if (type == 'premium') {
            channels = _.filter(channels, function(o) { return !o.list_price != '0.0'; });
            dispatch({ type: "FETCH_CHANNELS_FULFILLED", payload: channels })
        } else {
            dispatch({ type: "FETCH_CHANNELS_FULFILLED", payload: channels })
        }
    }
}


/**
 * Sorts the results
 * @data is the results array
 * @needle is the filter key 
 * @order is either ASC or DESC
 */
export function sortChannels(data, needle, order) {
    return function (dispatch) {

        var orderByField = function (needle) {
            return function (a, b) {
                var a = a[needle],
                    b = b[needle];

                if (order == "ASC") {
                    if (a < b) {
                        return -1;
                    } else if (a > b) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else if (order == "DESC") {
                    if (a > b) {
                        return -1;
                    } else if (a < b) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            };
        };

        needle = orderByField(needle);
        var toggledOrder = ''

        if (order == "ASC") {
            toggledOrder = "DESC";
        } else {
            toggledOrder = "ASC";
        }

        dispatch({ type: "SORT_CHANNELS_FULFILLED", payload: { data: data.sort(needle), sort: toggledOrder } })
    }
}