import axios from "axios";

export function updateCompany(id, company, auth_token) {
    return function (dispatch) {
        axios.patch(' https://api.gohiring.com/companies/' + id + '?auth_token=' + auth_token, {
            company
        },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "PATCH_COMPANY_FULFILLED", payload: response.data.company })
            })
            .catch((err) => {
                dispatch({ type: "PATCH_COMPANY_REJECTED", payload: err })
            });
    }
}