import axios from "axios";

export function fetchLocations(auth_token) {
    var session = [];
    if (!auth_token) {
        if (localStorage.session) {
            session = JSON.parse(localStorage.session);
            auth_token = session.auth_token;
        } else {
            return false;
        }
    }

    return function (dispatch) {
        axios.get('https://api.gohiring.com/locations?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_LOCATIONS_FULFILLED", payload: response.data.locations })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_LOCATIONS_REJECTED", payload: err })
            });
    }
}

export function saveLocation(locationData, auth_token) {
    var location = locationData;
    console.log(location);
    return function (dispatch) {
        axios.post(' https://api.gohiring.com:443/locations?auth_token=' + auth_token,
            { location },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "SAVE_LOCATIONS_FULFILLED", payload: response.data.location })


            })
            .catch((err) => {
                console.log(err);
                dispatch({ type: "SAVE_LOCATIONS_REJECTED", payload: err })
            });


    }
}

export function getCompany(locations, auth_token) {
    var session = [];
    if (!auth_token) {
        if (localStorage.session) {
            session = JSON.parse(localStorage.session);
            auth_token = session.auth_token;
        } else {
            return false;
        }
    }
    return function (dispatch) {
        axios.get('https://api.gohiring.com/companies/' + locations[0].company_id + '?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_COMPANY_FULFILLED", payload: response.data.company });
            })
    }
}

export function getBillingAddress(auth_token) {
    var session = [];
    if (!auth_token) {
        if (localStorage.session) {
            session = JSON.parse(localStorage.session);
            auth_token = session.auth_token;
        } else {
            return false;
        }
    }

    return function (dispatch) {
        axios.get('https://api.gohiring.com/locations?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_LOCATIONS_FULFILLED", payload: response.data.locations });
                dispatch(getCompany(response.data.locations, auth_token));
            })
            .catch((err) => {
                dispatch({ type: "FETCH_LOCATIONS_REJECTED", payload: err })
            });
    }
}

