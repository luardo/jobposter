/**
 * Created by luardo on 03/11/16.
 */
import dispatcher from "../dispatcher"
import axios from "axios";

export function login(authData) {

    axios.post(' https://api.gohiring.com:443/sessions', {
        email: authData.email,
        password: authData.password
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.gohiring+json;version=v2',
            'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
        }
    })
        .then(function (response) {
            dispatcher.dispatch({
                type: "AUTH_PASS", sessions:
                    response.data
            });
        })
        .catch(function (error) {
            dispatcher.dispatch({
                type: "AUTH_ERROR", sessions:
                    error.data
            });
        });
}