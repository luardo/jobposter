import axios from "axios";

export function addToBasket(jobId, channelIds) {
    return function (dispatch) {

        dispatch({
            type: "ADDED_TO_CART", payload: {
                job_id: jobId,
                channel_ids: channelIds
            }
        })
    }
}


export function createOrder(order, auth_token) {
    return function (dispatch) {
        axios.post(' https://api.gohiring.com/orders?auth_token=' + auth_token, {
            order
        },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "CREATE_ORDER_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "CREATE_ORDER_REJECTED", payload: err })
            });
    }
}

export function updateOrder(id, order, auth_token) {
    return function (dispatch) {
        axios.patch(' https://api.gohiring.com/orders/' + id + '?auth_token=' + auth_token, {
            order
        },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "UPDATE_ORDER_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "UPDATE_ORDER_REJECTED", payload: err })
            });
    }
}

export function hasOrder() {
    return function (dispatch) {
        if (localStorage.order) {
            let order = JSON.parse(localStorage.order);
            return order;
        } else {
            return false;
        }
    }

}

export function fetchOrders(auth_token) {
    return function (dispatch) {
        axios.get(' https://api.gohiring.com:443/orders?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_ORDERS_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_ORDERS_REJECTED", payload: err })
            });
    }
}

export function fetchOrderById(id, auth_token) {
    return function (dispatch) {
        axios.get(' https://api.gohiring.com:443/orders/' + id + '?auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_ORDER_FULFILLED", payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_ORDER_FULFILLED", payload: err })
            });
    }
}

export function getChannelsInOrder(ids, auth_token) {
    return function (dispatch) {
        console.log(ids);
        if (!ids || ids.length < 1) {
            dispatch({ type: "FETCH_CHANNELS_ORDER_FULFILLED", payload: [] })
            return false;
        }

        var params = ids.map(function (id) {
            return "ids[]=" + id;
        });

        params = params.join('&');
        axios.get(' https://api.gohiring.com/channels?' + params + '&auth_token=' + auth_token,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/vnd.gohiring+json;version=v2',
                    'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
                }
            })
            .then((response) => {
                dispatch({ type: "FETCH_CHANNELS_ORDER_FULFILLED", payload: response.data.channels })
            })
            .catch((err) => {
                dispatch({ type: "FETCH_CHANNELS_ORDER_REJECTED", payload: err })
            });
    }
}


