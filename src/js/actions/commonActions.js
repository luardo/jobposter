import axios from "axios";

export function getClassifications() {
  let session = [];
  if (localStorage.session) {
    session = JSON.parse(localStorage.session);
    return function (dispatch) {
      axios.get('https://api.gohiring.com/classifications?auth_token=HnVKTmDqsbDNRaqQm7HK',
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.gohiring+json;version=v2',
            'Authorization': 'kCwqkf76xXtK2W61qFDG9SFp3kTxK6T19FKfTdFzYaUmGp88y1hhzQrr'
          }
        })
        .then((response) => {
          dispatch({type: "FETCH_CLASSIFICATIONS_FULFILLED", payload: response.data.classifications})
        })
        .catch((err) => {
          dispatch({type: "FETCH_CLASSIFICATIONS_REJECTED", payload: err})
        });
    }
  }
}

export function getChannels() {
  return function (dispatch) {
    axios.get('https://api.gohiring.com:443/channels',
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.gohiring+json;version=v2',
          'Authorization': 'kCwqkf76xXtK2W61qFDG9SFp3kTxK6T19FKfTdFzYaUmGp88y1hhzQrr'
        }
      })
      .then((response) => {
        dispatch({type: "FETCH_CHANNELS_FULFILLED", payload: response.data.channels})
      })
      .catch((err) => {
        dispatch({type: "FETCH_CHANNELS_REJECTED", payload: err})
      });
  }
}

export function getSchedules() {
  return function (dispatch) {
    axios.get('https://api.gohiring.com:443/schedules',
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.gohiring+json;version=v2',
          'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
        }
      })
      .then((response) => {
        dispatch({type: "FETCH_SCHEDULES_FULFILLED", payload: response.data.schedules})
      })
      .catch((err) => {
        dispatch({type: "FETCH_SCHEDULES_REJECTED", payload: err})
      });
  }
}

export function getOccupations(occupation_list) {
  return function (dispatch) {
    dispatch({type: "FETCH_OCCUPATIONS_FULFILLED", payload: occupation_list})
  }
}

export function getCategories() {
  let session = [];
  if (localStorage.session) {
    session = JSON.parse(localStorage.session);

    return function (dispatch) {
      axios.get('https://api.gohiring.com:443/categories?auth_token=' + session.auth_token,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/vnd.gohiring+json;version=v2',
            'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
          }
        })
        .then((response) => {
          dispatch({type: "FETCH_CATEGORIES_FULFILLED", payload: response.data.categories})
        })
        .catch((err) => {
          dispatch({type: "FETCH_CATEGORIES_REJECTED", payload: err})
        });
    }

  }

}

export function getSeniorities() {
  return function (dispatch) {
    axios.get('https://api.gohiring.com:443/seniorities',
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.gohiring+json;version=v2',
          'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
        }
      })
      .then((response) => {
        dispatch({type: "FETCH_SENIORITIES_FULFILLED", payload: response.data.seniorities})
      })
      .catch((err) => {
        dispatch({type: "FETCH_SENIORITIES_REJECTED", payload: err})
      });
  }
}

export function getYearsOfExperience() {
  return function (dispatch) {
    axios.get('https://api.gohiring.com:443/years_of_experiences',
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.gohiring+json;version=v2',
          'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
        }
      })
      .then((response) => {
        console.log(response);
        dispatch({type: "FETCH_YEARS_OF_EXPERIENCE_FULFILLED", payload: response.data.years_of_experience})
      })
      .catch((err) => {
        dispatch({type: "FETCH_YEARS_OF_EXPERIENCE_REJECTED", payload: err})
      });
  }
}

export function getIndustries() {
  return function (dispatch) {
    axios.get('https://api.gohiring.com/industries',
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.gohiring+json;version=v2',
          'Authorization': 'gKt464q6bbs7Tj1ow5Mqm441yG74H9yCZcY1Xeq1ojWY5wVqUfDZMgrr'
        }
      })
      .then((response) => {
        dispatch({type: "FETCH_INDUSTRIES_FULFILLED", payload: response.data.industries})
      })
      .catch((err) => {
        dispatch({type: "FETCH_INDUSTRIES_REJECTED", payload: err})
      });
  }
}
