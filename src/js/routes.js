import React from "react";
import {Route, IndexRoute} from 'react-router'

import CreateJobPage from "./Pages/CreateJobPage";
import JobboardPage from "./Pages/JobboardPage";
import JobsListPage from "./Pages/JobsListPage";
import Layout from "./Pages/Layout";
import DashboardPage from "./Pages/DashboardPage";
import Login from "./Pages/Login";
import Logout from "./Pages/Logout";
import OrdersListPage from "./Pages/OrdersListPage";
import OrdersConfirmPage from "./Pages/ConfirmOrderPage";
import PaymentPage from "./Pages/PaymentPage";
import Signup from "./Pages/Signup";
import LocationPage from "./Pages/LocationPage";
import FrameContracts from "./Pages/FrameContracts";

const routes = (
  <Route path="/" component={Layout}>
    <IndexRoute component={DashboardPage}/>
    <Route path="/signup" name="Signup" component={Signup}/>
    <Route
      path="/contract"
      name="Frame Contract"
      component={FrameContracts}
    />
    <Route
      path="/jobs/add"
      name="Add job"
      navDisable
      component={CreateJobPage}
    />


    <Route
      path="/jobs"
      name="Add job"
      navDisable
      component={JobsListPage}
    />
    <Route
      path="/jobboards"
      name="Publish"
      navDisable
      component={JobboardPage}
    />
    <Route path="/location" name="location" component={LocationPage}/>

    <Route path="/login" name="Log in" component={Login}/>
    <Route path="/logout" name="Log out" component={Logout}/>

    <Route
      path="/dashboard"
      name="Dashboard"
      navDisable={false}
      component={DashboardPage}
    />
    <Route
      path="/publish/job/id/:jobId"
      name="Jobboard"
      navDisable
      component={JobboardPage}
    />
    <Route
      path="/checkout/payment/order/:orderId"
      navDisable
      name="Payment"
      component={PaymentPage}
    />

    <Route
      path="/order/confirm/:orderId"
      name="Order Confirm"
      component={OrdersConfirmPage}
    />
    <Route path="/orders" name="Orders" component={OrdersListPage}/>
  </Route>
);

export default routes;