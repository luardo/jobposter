/**
 * Created by luardo on 30/10/16.
 */
import React from "react";


import * as SignupActions from "../actions/SignupActions";
import SignupStore from "../Stores/SignupStore"
import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox,  } from 'react-bootstrap';


export default class CompanySizes extends React.Component {
    constructor() {
        super();
        this.getCompanySizes = this.getCompanySizes.bind(this);
        this.state = {
            company_sizes:[]
        };
        this.handleChange = this.handleChange.bind(this);

    }

    componentWillMount() {
        SignupStore.on("change", this.getCompanySizes);
        SignupActions.loadCompanySizes();
    }

    componentWillUnmount() {
        SignupStore.removeListener("change", this.getCompanySizes);
    }

    getCompanySizes() {
        this.setState({
            company_sizes: SignupStore.getCompanySizes()
        });
    }


    handleChange(event) {
        this.props.onHandleChange('company_size', event.target.value);
    }

    render() {

        const {company_sizes} = this.state;

        const companySizesOptions = company_sizes.map((option) => {
            return <option value={option.code} key={option.code}>{option.value}</option>;
        });
        return (
            <FormGroup controlId="company_size">
                <ControlLabel>Select your company size: {this.props.company}</ControlLabel>
                <FormControl bsClass="form-control  c-select" componentClass="select" placeholder="select" onChange={this.handleChange}>
                    <option value="">select</option>
                    {companySizesOptions}
                </FormControl>
            </FormGroup>
        );
    }

}