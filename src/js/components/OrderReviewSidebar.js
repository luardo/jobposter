import React from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getUserToken } from '../actions/loginActions'
import { addToBasket, fetchOrderById, getChannelsInOrder, hasOrder } from '../actions/orderActions'
import { forEach, union, isUndefined } from 'lodash'

import ChannelItem from "./Order/ChannelItem"
import BillingAddressWidget from './Order/BillingAddressWidget'

class OrderReviewSidebar extends React.Component {
    constructor() {
        super();
        this.getChannels = this.getChannels.bind(this);

    }

    componentWillMount() {
        const {order} = this.props;
        if (!order) { //if order is not in props then fetch the order
            let orderSession = this.props.hasOrder(); //if order exist in localstorage, loads order
            if (orderSession)
                this.props.fetchOrderById(orderSession.order_id, this.props.getUserToken());
        } else {
            this.getChannels(order.order.channel_ids);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.order) {
            if (nextProps.channels_fetched == false)
                this.getChannels(nextProps.order.order.channel_ids);
        }
    }

    getChannels(ids) {
        this.props.getChannelsInOrder(ids, this.props.getUserToken());
    }

    removeCartItem(id) {
        this.props.onChangeHandler(id);
    }

    render() {
        const {order} = this.props;

        if (order && this.props.channel_order.length > 0) {
            return (
                <div className="card-box">
                    <h3>Order Review </h3>
                    <hr />
                    <BillingAddressWidget />
                    <hr />
                    <h5>Your order </h5>


                    <table className="table ">
                        <ChannelItem channels={this.props.channel_order}
                            onChangeHandler={this.removeCartItem.bind(this)} />
                    </table>

                    <table className="table ">
                        <tbody>
                            <tr>
                                <td><strong>Subtotal</strong></td>
                                <td><strong>{order.order.subtotal}€</strong></td>
                            </tr>

                            <tr>
                                <td>Tax</td>
                                <td>{order.order.total_tax}€ ({order.order.tax_rate * 100} % )</td>
                            </tr>

                            <tr>
                                <td><strong>Grand Total</strong></td>
                                <td><strong>{order.order.total}€</strong></td>
                            </tr>
                        </tbody>
                    </table>

                    <a href="#" className="btn btn-block btn-lg btn-info waves-effect waves-light">Next Step ></a>
                    <br />
                </div>

            );
        } else {
            return (
                <div className="card-box">
                     <BillingAddressWidget />
                    <hr />
                    Select a job website where you want to publish your offer
                </div>
            )
        }


    }
}

function mapStateToProps(state) {
    return {
        order: state.order.order,
        channel_order: state.order.channel_order,
        channels_fetched: state.order.channels_fetched
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchOrderById: bindActionCreators(fetchOrderById, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch),
        hasOrder: bindActionCreators(hasOrder, dispatch),
        getChannelsInOrder: bindActionCreators(getChannelsInOrder, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderReviewSidebar);