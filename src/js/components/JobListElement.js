import React from "React"

import JobDescriptions from "./JobDescriptions"
import { Link } from 'react-router'
import dateFormat from 'dateformat'

export default class JobListElement extends React.Component {
    constructor() {
        super();
    }


    render() {
        const {details} = this.props;
        return (
            <div className="card-box job-posting" data-status="pagado" key={details.id}>
                <div className="media">

                    <div className="media-body">
                        <span className="media-meta pull-right"></span>
                        <h5 className="title">
                            {details.position}
                            <span className="pull-right pagado state">({details.state})</span>
                        </h5>
                        <div>
                            <span className="pull-right  state">
                                <i className="ion-calendar"></i>
                                {dateFormat(details.created_at, 'd mmm , yyyy')}
                            </span>

                        </div>
                        <div className="detail">
                            <ul>
                                <li><strong>Type of job: </strong>{details.schedule}</li>
                                <li><strong>Seniority level: </strong>{details.seniority}</li>
                                <li><strong>Classification: </strong>{details.classification}</li>
                            </ul>
                        </div>

                        <Link to={`/publish/job/id/${details.id}`}> Publish </Link>
                        <Link to={`/jobs/edit/${details.id}`}> Edit </Link>
                    </div>
                </div>
            </div>
        );
    }
}