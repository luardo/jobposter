/**
 * Created by luardo on 08/11/16.
 */
import React from "react";

import {ListGroup, ListGroupItem} from 'react-bootstrap';

export default class LocationsList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {locationsList} = this.props;

        if (locationsList) {
            const listItem = locationsList.reverse().map((option) => {
                return <ListGroupItem key={option.id}>{option.name} | {option.street} {option.postal_code}  {option.city} </ListGroupItem>;
            });

            return (
                <div>
                    <ListGroup>
                    {listItem}
                    </ListGroup>
                </div>
            )

        } else {
            return (
                <div>
                    <ListGroup>
                    </ListGroup>
                </div>
            )
        }

    }
}