import React from "react"

require("../scss/registerSteps.scss");

export default class RegisterSteps extends React.Component {
    render() {
        return (
            <div className="wizard clearfix">
                <div className="steps clearfix">
                    <ul role="tablist">
                        <li role="tab" className="first disabled" aria-disabled="false" aria-selected="true">
                            <a id="steps-uid-0-t-0" href="#steps-uid-0-h-0" aria-controls="steps-uid-0-p-0">
                                <span className="current-info audible">current step: </span><span className="number">1.</span>Account</a>
                        </li>
                        <li role="tab" className="current" aria-disabled="true">
                            <a id="steps-uid-0-t-1" href="#steps-uid-0-h-1" aria-controls="steps-uid-0-p-1"><span className="number">2.</span> Create job offer</a>
                        </li>
                        <li role="tab" className="disabled" aria-disabled="true">
                            <a id="steps-uid-0-t-2" href="#steps-uid-0-h-2" aria-controls="steps-uid-0-p-2"><span className="number">3.</span> Job boards list</a>
                        </li>
                        <li role="tab" className="disabled last" aria-disabled="true">
                            <a id="steps-uid-0-t-3" href="#steps-uid-0-h-3" aria-controls="steps-uid-0-p-3"><span className="number">4.</span> Publish</a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}