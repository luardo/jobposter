import React from "react"
import JobboardForm from "./JobboardForm"
import { filter } from 'lodash'

export default class JobboardChannels extends React.Component {
    constructor() {
        super();
        this.state = {
            filterBy: 'free'
        }
    }


    render() {
        const {activePage, channelsArray, filter, resultsPerPage } = this.props;
        const self = this;
        if (channelsArray) {
            const channelList = channelsArray.map(function (channel, index) {
                if (index >= resultsPerPage * (activePage - 1) && index < activePage * resultsPerPage) {
                    return <JobboardForm channel={channel} key={channel.id}
                        isSelected={self.props.isSelected.bind(self, channel.id)}
                        onHandleChange={self.props.onChangeHandler.bind(self)} />
                }
            });

            return (
                <div className="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                    {channelList}
                </div>
            );
        } else {
            return null;
        }

    }
}