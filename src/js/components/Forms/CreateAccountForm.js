/**
 * Created by luardo on 30/10/16.
 */
import React from "react";

import CompanySizes from "../../components/CompanySizes"
import Industries from "../../components/Industries"

import * as SignupActions from "../../actions/SignupActions";
import SignupStore from "../../Stores/SignupStore";

import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col } from 'react-bootstrap';

export default class CreateAccountForm extends React.Component {
    constructor() {
        super();
    }

    render() {
        const {signup} = this.props;

        return (
            <form>
                <legend>Create Account</legend>

                
                <div className="row">
                    <Col xs={12} md={6}>
                        <FormGroup controlId="first_name">
                            <ControlLabel>First Name {signup.first_name}</ControlLabel>
                            <FormControl type="text" value={signup.first_name} placeholder="Enter first name"
                                onChange={this.props.onChange.bind(this, 'first_name')} />
                            <FormControl.Feedback />
                        </FormGroup>
                    </Col>
                    <Col xs={12} md={6}>
                        <FormGroup controlId="last_name">
                            <ControlLabel>Last Name</ControlLabel>
                            <FormControl type="text" value={signup.last_name} placeholder="Enter your last name"
                                onChange={this.props.onChange.bind(this, 'last_name')} />
                            <FormControl.Feedback />
                            <HelpBlock>Validation is based on string length.</HelpBlock>
                        </FormGroup>
                    </Col>

                </div>

                <div className="row">
                    <Col xs={12} md={6}>
                        <FormGroup controlId="email">
                            <ControlLabel>Email</ControlLabel>
                            <FormControl type="email" value={signup.email} placeholder="Enter your email"
                                onChange={this.props.onChange.bind(this, 'email')} />
                            <FormControl.Feedback />
                            <HelpBlock>Validation is based on string length.</HelpBlock>
                        </FormGroup>
                    </Col>
                    <Col xs={12} md={6}>
                        <FormGroup controlId="company_name">
                            <ControlLabel>Company Name</ControlLabel>
                            <FormControl type="text" value={signup.company_name} placeholder="Enter your company name"
                                onChange={this.props.onChange.bind(this, 'company_name')} />
                            <FormControl.Feedback />
                            <HelpBlock>Validation is based on string length.</HelpBlock>
                        </FormGroup>
                    </Col>
                </div>

                <div className="row">
                    <Col xs={12} md={6}>
                        <FormGroup controlId="password">
                            <ControlLabel>Password </ControlLabel>
                            <FormControl type="password" value={signup.password} placeholder="Enter your password"
                                onChange={this.props.onChange.bind(this, 'password')} />
                            <FormControl.Feedback />
                        </FormGroup>
                    </Col>
                    <Col xs={12} md={6}>
                        <FormGroup controlId="password_confirmation">
                            <ControlLabel>Confirm password </ControlLabel>
                            <FormControl type="password" value={signup.password_confirmation} placeholder="confirm your password"
                                onChange={this.props.onChange.bind(this, 'password_confirmation')} />
                            <FormControl.Feedback />
                        </FormGroup>
                    </Col>
                </div>
                <Button bsStyle="success" onClick={this.props.onCreateSignup.bind(this)}>Create Free Account</Button>
            </form >
        );
    }
}