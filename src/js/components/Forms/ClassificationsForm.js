/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";
import {FormGroup, FormControl, ControlLabel, FieldGroup} from 'react-bootstrap';

export default class ClassificationsForm extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      classifications: []
    };
  }

  handleChange(event) {
    this.props.onHandleChange('classification', event.target.value);
  }

  render() {
    const {classifications, selected} = this.props;

    const SelectOptions = classifications.map((option) => {
      return <option value={option.code} key={option.code}>{option.value}</option>;
    });
    return (
      <FormGroup controlId="classifications">
        <ControlLabel>Select job classification: </ControlLabel>
        <FormControl componentClass="select" value={selected} bsClass="form-control c-select" placeholder="select"
                     onChange={this.handleChange}>
          <option value="">select</option>
          {SelectOptions}
        </FormControl>
      </FormGroup>
    );
  }

}