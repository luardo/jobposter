import React from "react";
import {FormGroup, FormControl, ControlLabel, FieldGroup} from 'react-bootstrap';


export default class PaymentForm extends React.Component {
  constructor() {
    super();
  }


  render() {

    return (
      <form>
        <h3>Invoice Address</h3>
        <FormGroup controlId="street">
          <ControlLabel>Street</ControlLabel>
          <FormControl type="text" value={this.props.paymentData.street} placeholder="street"
                       onChange={this.props.onChange.bind(this, 'street')}/>
        </FormGroup>
        <FormGroup controlId="city">
          <ControlLabel>city</ControlLabel>
          <FormControl type="text" value={this.props.paymentData.city} placeholder="city"
                       onChange={this.props.onChange.bind(this, 'city')}/>
        </FormGroup>
        <FormGroup controlId="postal_code">
          <ControlLabel>postal_code</ControlLabel>
          <FormControl type="text" value={this.props.paymentData.postal_code} placeholder="postal_code"
                       onChange={this.props.onChange.bind(this, 'postal_code')}/>
        </FormGroup>
        <FormGroup controlId="country">
          <ControlLabel>country</ControlLabel>
          <FormControl type="text" value={this.props.paymentData.country} placeholder="country"
                       onChange={this.props.onChange.bind(this, 'country')}/>
        </FormGroup>

      </form>
    );
  }

}