/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";


import * as CreateJobActions from "../../actions/CreateJobActions";
import CreateJobStore from "../../Stores/CreateJobStore"

import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox,  } from 'react-bootstrap';


export default class ChannelsForm extends React.Component {
    constructor() {
        super();
        this.getChannels = this.getChannels.bind(this);
        this.state = {
            channels:[]
        };
    }

    componentWillMount() {
        CreateJobActions.on("change", this.getChannels);
        CreateJobActions.getChannels();
    }

    componentWillUnmount() {
        CreateJobActions.removeListener("change", this.getChannels);
    }

    getChannels() {
        this.setState({
            channels: CreateJobActions.getChannels()
        });
    }


    render() {

        const {channels} = this.state;

        const channelsOptions = channels.map((option) => {
            return <option value={option.id} key={option.code}>{option.name} Price: {option.sales_price}</option>;
        });
        return (
            <FormGroup controlId="channels">
                <ControlLabel>Select publishing Channel: {this.props.job.channels}</ControlLabel>
                <FormControl componentClass="select" placeholder="select" onChange={this.handleChange}>
                    <option value="">select</option>
                    {channelsOptions}
                </FormControl>
            </FormGroup>
        );
    }

}

