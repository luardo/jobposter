import React from "react"

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { FormGroup, FormControl, ControlLabel, FieldGroup } from 'react-bootstrap';


class JobDescriptionsForm extends React.Component {
    constructor(props) {
        super(props);

        this.index = this.getPosInArray(props.fieldName, props.descriptions_attributes, 'description_type');
        var descriptionAttributes = props.descriptions_attributes[this.index];


        this.state = {
            "descriptions_attributes": [
                {
                    "description_type": descriptionAttributes.description_type,
                    "description": descriptionAttributes.description
                },
            ]
        };
    }


    getPosInArray(needle, array, node) {
        var length = array.length;
        for (var i = 0; i < length; i++) {
            if (array[i][node] == needle) return i;
        }
    }

    handleChange(event) {
        const descriptions_attributes = {
            "description_type": this.props.fieldName,
            "description": event.target.value
        };

        this.props.onHandleChange('descriptions_attributes', descriptions_attributes);
    }

    render() {
        const {descriptions_attributes} = this.props;
        return (
            <FormGroup controlId={this.props.fieldName}>
                <ControlLabel>{this.props.label} </ControlLabel>
                <FormControl rows="3" componentClass="textarea" placeholder="textarea" onChange={this.handleChange.bind(this)}
                    value={descriptions_attributes[this.index].description} />
            </FormGroup>
        );
    }
}

function mapStateToProps(state) {
    return {
        job: state.jobs.job
    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JobDescriptionsForm)