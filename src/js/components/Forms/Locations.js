/**
 * Created by luardo on 30/10/16.
 */
import React from "react";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getUserToken } from "../../actions/loginActions";
import { fetchLocations } from "../../actions/locationActions";

import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, } from 'react-bootstrap';


class Locations extends React.Component {
    constructor() {
        super();
        this.state = {
            locations: [],
            session: [],
            locationSet: false
        };
        this.handleChange = this.handleChange.bind(this);


    }

    componentWillMount() {
        this.props.fetchLocations(this.props.getUserToken());
    }


    componentWillReceiveProps(nextProps) {
        const {locationSet} = this.state;
        if (nextProps.locations && locationSet == false) {
            if (nextProps.locations.length == 1) { //set location in state if there is only one location
                this.props.onHandleChange('location_id', nextProps.locations[0].id);
                this.setState({
                    locationSet: true
                });
            }
        }
    }



    handleChange(event) {
        this.props.onHandleChange('location_id', event.target.value);
    }

    render() {

        const {locations} = this.props;

        if (locations) {
            const options = locations.map((option) => {
                return <option value={option.id} key={option.id}>{option.name} - {option.street} {option.postal_code}</option>;
            });

            if (options.length > 1) {
                return (
                    <FormGroup controlId="locations">
                        <ControlLabel>Select your location: </ControlLabel>
                        <FormControl componentClass="select" placeholder="select" onChange={this.handleChange}>
                            <option value="">select</option>
                            {options}
                        </FormControl>
                    </FormGroup>
                );
            } else if (options.length == 1) {
                return (
                    <FormGroup controlId="locations">
                        <FormControl type="hidden" value={locations[0].id}
                            onChange={this.handleChange} />
                        <FormControl.Feedback />
                    </FormGroup>
                );
            } else {
                return (
                    <p>No locations</p>
                );
            }
        }

        else {
            return null;
        }


    }

}

function mapStateToProps(state) {
    return {
        locations: state.location.locations,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchLocations: bindActionCreators(fetchLocations, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Locations)