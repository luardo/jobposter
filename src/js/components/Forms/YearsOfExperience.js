/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";


import * as CreateJobActions from "../../actions/CreateJobActions";
import CreateJobStore from "../../Stores/CreateJobStore"

import {FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox,} from 'react-bootstrap';


export default class YearsOfExperience extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onHandleChange('years_of_experience', event.target.value);
  }

  render() {

    const {years_of_experience, selected} = this.props; //this is the collection of all options for years_of_experience
    //{this.props.years_of_experience} is the value in this.props from the existing job instance

    const SelectOptions = years_of_experience.map((option) => {
      return <option value={option.code} key={option.code}>{option.value}</option>;
    });
    return (
      <FormGroup controlId="years_of_experience">
        <ControlLabel>Select required Years of Experience: </ControlLabel>
        <FormControl bsClass="form-control c-select" value={selected} componentClass="select" placeholder="select"
                     onChange={this.handleChange}>
          <option value="">select</option>
          {SelectOptions}
        </FormControl>
      </FormGroup>
    );
  }

}