/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import CreateJobStore from "../../Stores/CreateJobStore"

import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, } from 'react-bootstrap';

import { getCategories, getOccupations } from "../../actions/commonActions";


class Categories extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.getOccupationFromCategory = this.getOccupationFromCategory.bind(this);

    }

    componentWillMount() {
        this.props.getCategories();
    }

    componentWillUnmount() {

    }

    getOccupationFromCategory(event) {
        const {categories} = this.props;
        var occupations = []

        for (let i = 0; i < categories.length; i++) {

            categories[i];
            if (categories[i].code == event.target.value) {
                occupations = categories[i].value.occupations;
            }
        }

        this.props.getOccupations(occupations)

    }

    handleChange(event) {
        this.props.onHandleChange('category', event.target.value);
        this.getOccupationFromCategory(event);
    }


    render() {

        const {categories} = this.props;
        if (categories) {

            const SelectOptions = categories.map((option) => {
                return <option value={option.code} key={option.code}>{option.value.name}</option>;
            });
            return (
                <FormGroup controlId="category">
                    <ControlLabel>Select publishing Category: </ControlLabel>
                    <FormControl bsClass="form-control c-select" componentClass="select" placeholder="select" onChange={this.handleChange} >
                        <option value="">select</option>
                        {SelectOptions}
                    </FormControl>
                </FormGroup>
            );
        }
    }

}

function mapStateToProps(state) {
    return {
        categories: state.common.categories,
        occupation_list: state.common.occupation_list
    }
}


function mapDispatchToProps(dispatch) {
    return {
        getCategories: bindActionCreators(getCategories, dispatch),
        getOccupations: bindActionCreators(getOccupations, dispatch),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Categories)