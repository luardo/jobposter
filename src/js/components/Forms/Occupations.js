/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as CreateJobActions from "../../actions/CreateJobActions";
import CreateJobStore from "../../Stores/CreateJobStore"

import { getOccupations } from "../../actions/commonActions";


import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, } from 'react-bootstrap';


class Occupations extends React.Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this.props.getOccupations;
    }


    handleChange(event) {
        var arr = new Array();
        arr.push(event.target.value);
        this.props.onHandleChange('occupation_list', arr);
    }

    render() {

        const {occupation_list} = this.props;
        var occupations = [];

        if (occupation_list != undefined) {
            const SelectOptions = $.map(occupation_list, function (item, key) {
                return <option value={key} key={key}>{item}</option>;
            });
            return (
                <div >
                    <FormGroup controlId="occupation_list">
                        <ControlLabel>Select Occupations: </ControlLabel>
                        <FormControl bsClass="form-control c-select" componentClass="select" placeholder="select" onChange={this.handleChange}>
                            <option value="">select</option>
                            {SelectOptions}
                        </FormControl>
                    </FormGroup>
                </div>
            );
        }
    }

}

function mapStateToProps(state) {
    return {
        occupation_list: state.common.occupation_list
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getOccupations: bindActionCreators(getOccupations, dispatch),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Occupations)