/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";
import {FormGroup, ControlLabel, FieldGroup,} from 'react-bootstrap';

export default class SenioritiesForm extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onHandleChange('seniority', event.target.value);
  }

  render() {

    const {seniorities} = this.props;
    if (seniorities) {
      const SelectOptions = seniorities.map((option) => {

        return <div className="radio radio-info radio-inline" key={option.code}>
          <input type="radio" id={option.code} value={option.code} name="seniorities"/>
          <label for={option.code}> {option.value}</label>
        </div>;
      });

      return (
        <FormGroup controlId="seniorities">

          <ControlLabel>Select type of job level: </ControlLabel>
          <div class="button-list">
            {SelectOptions}
          </div>

        </FormGroup>
      );
    } else {
      return null;
    }
  }

}