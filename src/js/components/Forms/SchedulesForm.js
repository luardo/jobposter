/**
 * Created by luardo on 01/11/16.
 */
/**
 * Created by luardo on 30/10/16.
 */
import React from "react";
import {FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox,} from 'react-bootstrap';

export default class SchedulesForm extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      schedules: []
    };
  }

  handleChange(event) {
    this.props.onHandleChange('schedule', event.target.value);
  }

  render() {

    const {schedules, selected} = this.props;

    const SelectOptions = schedules.map((option) => {
      return <option value={option.code} key={option.code}>{option.value}</option>;
    });
    return (
      <FormGroup controlId="schedules">
        <ControlLabel>Select type of contract:</ControlLabel>
        <FormControl bsClass="form-control c-select" value={selected} componentClass="select" placeholder="select"
                     onChange={this.handleChange}>
          <option value="">select</option>
          {SelectOptions}
        </FormControl>
      </FormGroup>
    );
  }
}