/**
 * Created by luardo on 01/11/16.
 */
import React from "react";
import Categories from "./Categories"
import JobDescriptionsForm from './JobDescriptionsForm'
import Industries from "../Industries"
import Locations from "./Locations"
import Occupations from "./Occupations"

import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col } from 'react-bootstrap';

export default class CreateJobForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {job} = this.props;
        return (
            <form>
                <FormGroup controlId="position">
                    <ControlLabel>Name of the job position</ControlLabel>
                    <FormControl type="text" value={job.position} placeholder="position name"
                        onChange={this.props.onChange.bind(this, 'position')} />
                    <FormControl.Feedback />
                </FormGroup>

                <JobDescriptionsForm fieldName="job" label="Job Description"
                    descriptions_attributes={job.descriptions_attributes}
                    onHandleChange={this.props.onHandleChange.bind(this)} />

                <JobDescriptionsForm fieldName="qualifications" label="Qualifications"
                    descriptions_attributes={job.descriptions_attributes}
                    onHandleChange={this.props.onHandleChange.bind(this)} />

                <JobDescriptionsForm fieldName="additional_information" label="Additional Information"
                    descriptions_attributes={job.descriptions_attributes}
                    onHandleChange={this.props.onHandleChange.bind(this)} />

                <Locations company={job.location} onHandleChange={this.props.onHandleChange.bind(this)} />
                <Industries industry={job.industry} onHandleChange={this.props.onHandleChange.bind(this)} />
                <Categories category={job.category} onHandleChange={this.props.onHandleChange.bind(this)} />
                <Occupations occupation_list={job.occupation_list} onHandleChange={this.props.onHandleChange.bind(this)} />

                <FormGroup controlId="apply_url">
                    <ControlLabel>Job url</ControlLabel>
                    <FormControl type="text" value={job.apply_url} placeholder="url of the job offer in your website"
                        onChange={this.props.onChange.bind(this, 'apply_url')} />
                    <FormControl.Feedback />
                </FormGroup>

                <FormGroup controlId="apply_email">
                    <ControlLabel>Your email</ControlLabel>
                    <FormControl type="text" value={job.apply_email} placeholder="The email that applicants may apply through"
                        onChange={this.props.onChange.bind(this, 'apply_email')} />
                    <FormControl.Feedback />
                </FormGroup>
            </form>
        );
    }
}