import React from "react";
import { Link } from "react-router";

export default class NavBar extends React.Component {
  render() {
    const { navigationEnabe } = this.props;
    if (navigationEnabe) {
      return (
        <div className="navbar-custom">
          <div className="container">
            <div id="navigation">

              <ul className="navigation-menu">
                <li>
                  <Link to="/dashboard">
                    <i className="zmdi zmdi-view-dashboard" />
                    {" "}
                    <span> Dashboard </span>
                    {" "}
                  </Link>
                </li>
                <li className="has-submenu active">
                  <Link to="/jobs">
                    <i className="ion-briefcase" /> <span> Jobs </span>{" "}
                  </Link>
                </li>

                <li className="has-submenu">
                  <Link to="/jobs/add">
                    <i className="zmdi zmdi-collection-text" />
                    {" "}
                    <span> Create new job </span>
                    {" "}
                  </Link>

                </li>

                <li className="has-submenu">
                  <Link to="/jobboards">
                    <i className="zmdi zmdi-format-list-bulleted" />
                    <span> Job boards </span>
                    {" "}
                  </Link>

                </li>

                <li className="has-submenu">
                  <Link to="/orders">
                    <i className="ion-paperclip" />
                    {" "}
                    <span> All Orders </span>
                    {" "}
                  </Link>

                </li>

                <li className="has-submenu">
                  <Link>
                    {" "}
                    <i className="zmdi zmdi-chart" />
                    <span> Login </span>
                    {" "}
                  </Link>

                </li>

              </ul>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
