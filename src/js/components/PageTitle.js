import React from "react"
import RegisterSteps from "./RegisterSteps"

export default class PageTitle extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-sm-12 text-xs-center">
                    <RegisterSteps />
                    <h3 className="m-b-30 m-t-20">{this.props.title}</h3>
                </div>
            </div>
        )
    }
}