import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getChannel } from '../actions/channelsActions'
import { getUserToken } from '../actions/loginActions'

import { forEach, union } from 'lodash'

class OrderListChannels extends React.Component {
    constructor() {
        super();
        this.state = {
            'channel_array': []
        }

    }

    componentWillMount() {
        const {channel_ids} = this.props;
        const self = this;
        if (channel_ids) {
            _.forEach(channel_ids, function (id) {
              //  if (self.props.channels.fetching != true)
                //    self.props.getChannel(id, self.props.getUserToken());
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        var channel_array = this.state;



        if (nextProps.channels.id) {
            var channels = _.union(channel_array, nextProps.channels);
            this.setState({
                'channel_array': channels
            });

        }
    }

    getChannelAttr(id, attr) {
       return this.props.getChannelAttribute(id, attr, this.props.getUserToken());
    }


    render() {

        const { channel_ids } = this.props;
        const {channel_array} = this.state;
        const {self} = this;
        if (channel_ids) {
            const channelName = channel_ids.map(function (index, id) {
                return <span key={index}>{ () => {this.getChannelAttr(id , 'name')} }</span>
            });

            return (
                <div>
                    {channelName}
                </div>
            );
        } else {
            return (
                <div>
                    NO name
                </div>
            );
        }





    }

}

function mapStateToProps(state) {
    return {
        channels: state.channels.channels,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getChannel: bindActionCreators(getChannel, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderListChannels)