import React from "react"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import JobDescriptionsForm from './Forms/JobDescriptionsForm'
import { getUserToken } from '../actions/loginActions'
import { fetchJobDescription } from "../actions/jobsActions"

class JobDescription extends React.Component {

    constructor() {
        super();
    }

    componentWillMount() {
        const {description_ids} = this.props;
        this.props.fetchJobDescription(this.props.getUserToken(), description_ids);
    }

    render() {
        const {jobDescriptions} = this.props;

        if (jobDescriptions) {
            const descriptionsText = jobDescriptions.map(function (description) {
                return <div key={description.id}><strong>{description.description_type}</strong> <br />
                    <div dangerouslySetInnerHTML={{ __html: description.description }}></div></div>
            })
            return (
                <div>{descriptionsText}</div>
            );
        } else {
            return (
                <span>no description</span>
            );
        }

    }
}



function mapStateToProps(state) {
    return {
        jobDescriptions: state.jobs.jobDescriptions,
        session: state.login.session
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchJobDescription: bindActionCreators(fetchJobDescription, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JobDescription)