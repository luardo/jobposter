import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import JobListElement from './JobListElement'
import { fetchJobs } from '../actions/jobsActions'
import { getUserToken } from '../actions/loginActions'
import PageTitle from './PageTitle'

import { FormGroup, Panel, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox, Button, Col, ListGroup, ListGroupItem } from 'react-bootstrap';


class JobsList extends React.Component {
    constructor() {
        super();

    }

    componentWillMount() {
        this.props.fetchJobs(this.props.getUserToken());
    }



    render() {
        const {jobs} = this.props;

        if (jobs) {
            const jobslist = jobs.map((option) => {
                return <JobListElement details={option} key={option.id} />;
            });

            return (
                <div>
                    <PageTitle title="All job openings" />

                    <div className="row">
                        <div className="col-sm-12 col-xl-10 col-xl-offset-1">
                            <div className="row">
                                <div className="col-xs-12 ">

                                    {jobslist}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <h3>No jobs added</h3>
        );

    }

}

function mapStateToProps(state) {
    return {
        jobs: state.jobs.jobs,
        session: state.login.session
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchJobs: bindActionCreators(fetchJobs, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JobsList)