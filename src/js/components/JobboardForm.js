import React from "react"

import { FieldGroup, Button } from 'react-bootstrap';

export default class JobboardForm extends React.Component {
    constructor() {
        super();
    }

    render() {
        const {channel} = this.props;
        var isSelectedText = "btn-info";

        if (this.props.isSelected(channel.id)) {
            isSelectedText = "btn-warning";
        }

        return (
            <div key={channel.id} className="col-sm-4 col-lg-4 col-xl-3 col-xs-6">
                <div className="card">
                    <img className="card-img-top img-fluid" src={"https://api.gohiring.com/images/" + channel.logo_square_uid} alt="242x200" />

                    <div className="card-block">
                        <h4 className="card-title">{channel.name}</h4>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item"> {channel.category_list.join()} </li>
                        <li className="list-group-item">{channel.list_price} {channel.currency_code}</li>
                    </ul>
                    <div className="card-block">
                        <Button bsSize="small" bsClass={isSelectedText + " btn btn-sm waves-effect waves-light"}  
                        onClick={() => this.props.onHandleChange(channel.id)}>Select</Button>
                    </div>
                </div>
            </div>
        );
    }
}