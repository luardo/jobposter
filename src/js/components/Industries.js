/**
 * Created by luardo on 30/10/16.
 */
import React from "react";

import * as SignupActions from "../actions/SignupActions";
import SignupStore from "../Stores/SignupStore"
import { FormGroup, FormControl, HelpBlock, ControlLabel, FieldGroup, Radio, Checkbox,  } from 'react-bootstrap';

export default class Industries extends React.Component {

    constructor() {
        super();
        this.getIndustries  = this.getIndustries.bind(this);
        this.state = {
            industries:[]
        };
        this.handleChange = this.handleChange.bind(this);

    }

    componentWillMount() {
        SignupStore.on("change", this.getIndustries);
        SignupActions.loadIndustries();
    }

    componentWillUnmount() {
        SignupStore.removeListener("change", this.getIndustries);
    }

    getIndustries() {
        this.setState({
            industries: SignupStore.getIndustries()
        });
    }


    handleChange(event) {
        this.props.onHandleChange('industry', event.target.value);
    }

    render() {

        const {industries} = this.state;

        const IndustriesOptions = industries.map((option) => {
            return <option value={option.code} key={option.code}>{option.value}</option>;
        });
        return (
            <FormGroup controlId="company_size">
                <ControlLabel>Select your Industrie {this.props.industry}</ControlLabel>
                <FormControl componentClass="select" bsClass="form-control c-select" placeholder="select" onChange={this.handleChange}>
                    <option value="">select</option>
                    {IndustriesOptions}
                </FormControl>
            </FormGroup>
        );
    }




}