import React from 'react'

export default class ChannelItem extends React.Component {
    constructor() {
        super();
        this.onClickRemove = this.onClickRemove.bind(this);
    }

    onClickRemove(id) {
       this.props.onRemove(id);
    }

    render() {
        const {channels} = this.props;
        const self = this;

        if (channels) {
            const channelsRow = channels.map(function (channel) {
                return <tr key={channel.id}>
                    <td>{channel.name}</td>
                    <td>{channel.list_price}€</td>
                    <td><i className="ion-close" onClick={self.props.onChangeHandler.bind(self, channel.id)} ></i></td>
                </tr>;
            });
            return (
                <tbody>
                    {channelsRow}
                </tbody>

            )
        } else {
            <tr><td>No itema</td></tr>
        }

    }
}