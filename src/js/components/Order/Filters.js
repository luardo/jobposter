import React, { Component } from 'react';
import { Col } from 'react-bootstrap'

require("../../scss/filters.scss");

const active = (appliedFilters, attribute, value) => {
  return appliedFilters[attribute] && (appliedFilters[attribute].indexOf(value) > -1);
};

export default class Filters extends Component {

  applyFilter(attribute, value) {
    this.props.toggleFilter(attribute, value);
    this.props.setActivePage();
  }

  renderOptionGroup(group) {
    const { toggleFilter, appliedFilters } = this.props;
    return group.map((item, idx) => {
      const { attribute, value } = item;
      const isActive = active(appliedFilters, attribute, value);
      const style = {
        background: isActive ? 'nav-link active' : 'nav-link'
      };

      const styleClass = isActive ? 'btn-info ' : 'btn-secondary ';

      return <button type="button" className={styleClass + "btn  waves-effect "} key={idx} onClick={this.applyFilter.bind(this, attribute, value)}>
        {item.value}
      </button>;
    });
  }

  searchKeword(e) {
    this.props.keywordSearch(e.target.value);
  }

  sortItems() {
    const { sortItems, applySort } = this.props;
    const handleSortChange = (e) => {
      if (!e.target.value) return;
      const idx = e.target.value;
      applySort(sortItems[idx]);
    };
    return <select onChange={(e) => handleSortChange(e)} >
        <option value="" disabled>Sort Functions</option>
      {sortItems.map((item, idx) => {
        return <option key={idx} value={idx}>{item.title}</option>;
      })}
    </select>;
  }

  render() {

    const { optionGroups, clearAllFilters } = this.props;
    const items = optionGroups.map((group, idx) => {
      const { title, values } = group;
      return <div key={idx} className="btn-group m-b-20">
          <div className="item title" >{title}: </div>
        {this.renderOptionGroup((values))}
      </div>;
    });
    return (
      <div className="filters">


          <Col md={4} xs={12}>
              <input type="text" className="form-control" id="exampleInputEmail1" placeholder="Search ..." onChange={this.searchKeword.bind(this)} />
          </Col>
          <Col md={4} xs={6}>
            {items}
          </Col>
          <Col md={4}  xs={6}>
              <button className="btn btn-secondary" onClick={() => clearAllFilters()}>
                  <span>Clear All Filters </span>
                  <i className="fa fa-times"></i>
              </button>
          </Col>

      </div>
    );
  }
}