import React from 'react'

export default class OrderTotals extends React.Component {
  constructor() {
    super();
  }

  render() {
    const {order} = this.props;

    if (order) {
      return (
        <table className="table ">
          <tbody>
          <tr>
            <td><strong>Subtotal</strong></td>
            <td><strong>{order.subtotal}€</strong></td>
          </tr>

          <tr>
            <td>Tax</td>
            <td>{order.total_tax}€ ({order.tax_rate * 100} % )</td>
          </tr>

          <tr>
            <td><strong>Grand Total</strong></td>
            <td><strong>{order.total}€</strong></td>
          </tr>
          </tbody>
        </table>
      )
    } else {
      return null;
    }

  }
}