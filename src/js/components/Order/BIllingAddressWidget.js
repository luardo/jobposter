import React from 'react'
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getBillingAddress } from '../../actions/locationActions'
import { getUserToken } from '../../actions/loginActions'


class BillingAddressWidget extends React.Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.getBillingAddress(null);
    }

    render() {
        const {locations, fetched, company_fetched, company} = this.props;
        if (fetched == true && company_fetched == true) {
            return (
                <div>
                    <h5>Invoice address </h5>
                    <p>{company.name}
                        <br />{locations[0].street}
                        <br />{locations[0].postal_code} {locations[0].city}

                    </p>
                </div>
            )
        } else {
            return (
                <div>
                    No billing address
            </div>
            )
        }

    }
}

function mapStateToProps(state) {
    return {
        locations: state.location.locations,
        fetched: state.location.fetched,
        company: state.order.company,
        company_fetched: state.order.fetched
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getBillingAddress: bindActionCreators(getBillingAddress, dispatch),
        getUserToken: bindActionCreators(getUserToken, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BillingAddressWidget);