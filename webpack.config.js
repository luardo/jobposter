var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  context: path.join(__dirname, "src"),
  devtool: 'eval',
  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    '/'
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['react-hot', 'babel?presets[]=react,presets[]=stage-0,presets[]=es2015'],
        include: path.join(__dirname, 'src')

      },
      // Optionally extract less files
      // or any other compile-to-css language
      {
        test: /\.scss$/,
        include: path.join(__dirname, "src"),
        loaders: ["style-loader", "css-loader", "sass-loader"]
      }
    ]
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: "app.min.js"
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: 'React Foundation Components CSS Modules Example',
      template: 'my-index.ejs',
      inject: 'body'
    }),
  ]
};


